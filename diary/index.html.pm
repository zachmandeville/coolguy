#lang pollen

◊(title "Building up" "20 March 2020")


New Zealand is in full lock-down and it put me in a panic that we were going to be sent home, which felt bizarre nearly instantly-- why would we be virus carriers simply because we aren't Kiwi?  The country is trying to be practical, this feels good, I need to not internalize this feeling that I'm dangerous just because I'm not from here.

Watched some tutorials tonight on some group video conferencing software for holding classes with differnet rooms and all that.  I am keen to lean into this digital closeness more, but I also find that I am watching videos and reading up on code because I'm avoiding talking of my feeeeelings, and have this hesitation to open up at all on the future.  This muteness I feel isn't good, but I also know that I'm doing some sort of coping strategy that shows itself in these boring, practical interests.  I could write about my feelings, or I could learn how to batch rename a list of torrented television titles using lisp.  My feelings are large and scary, and code is a bunch of self-contained functions in a quiet black box with every edge defined.  So I read up on how to code better, and how to make software nicer, with reassuring functions that will always do what is epxected.   And I feel a quiet start in me, a quiet that makes me want to open up more.

◊mood{glum}

◊|end|

◊(title "Tantrums"
"16 March 2020")

In Tauranga in my favorite micro house airBnB, hopped up on two fun size mars bars and ranting to Angelica online about how the arc of Steven Universe didn't go the way I thought it should go, and I blame my generation.


◊(title "Another Nice Day"
"9 January 2020")

Lake South was playing tonight at the Botanical Gardens so Angelica and I made a trip on the 2 line to see them.

We've been in Wellington near two years now, and I have this calm affection for the place-- not yet the comfort of a local, but getting there.  It's an affection gained by familiar surroundings strengthened with memory.  Like how everytime I take the two bus I think of the trip when we sat behind two 12 year old boys in their school suits having a joyous conversation about cheese.  They were listing the gourmet cheeses they'd eaten, and marvelling about some recent camembert's depth of flavor.  They got so swept up by the memory of eating this cheese that they missed their stop, and the embarassed way they hit the request button and announced their blunder implied that, for them, losing track of time when talking about cheese was a commmon problem.

I've grown accustomed to the local whimsy and gentle surprises of this place- like, we knew the show would be pleasant and charming, but didn't know how pleasant or what charms.

Getting off the bus and I can already hear the music, a stream of people moving through the open gates, or stopping at the tip top cart parked right outside.   An opening band was playing soft-voiced acoustic music, that sincere type that always sounds like the band is dying of some heirloom illness, struggling to get the words out, overcome by the fragile beauty of it all.  Not really my thing, but I loooved the crowd of people on picnic blankets in the grass, and how they filled the steep hills surrounding the sound shell, all their butts parked strategically in divets in the grass, their blankets at an incline, cases of tuatara and tui next to all of them, the whole bunch looking like lightly buzzed billy goats.

In front of the stage was a square of asphalt set up for dancing.  When Lake South started to play there were three dancers, all dudes, feeling the music and their bodies.  They danced in the slow motion wind puppet style, like a chaotic tae chi.

The self-evident leader of the dancers was a short man in tight jeans and white shoes, with an open waist-cut jacket and a tucked white shirt.  He danced with incredible, esoteric footwork--each movement intentional, but without tradition. At times it seemed like he was doing a cha cha, then a charleston, then a set of movements that felt symbolically important, but impenetrable.

A woman in a tiny backpack and fancy felt hat joined the trio quickly, bringing her own individual style of dance, so now there were four people who were united by each being lost in their own world.  And of course the dancing was silly from a distance, and with the sun still up and the asphalt sparse it meant all eyes were on them. But it just gave them this noble sort of awkward, like I was so happy to see people just feeling themselves and the music.

Lake South is an super dancy band though, with just the right amount of corniness in their sound that you wanna dance foolishly, and a city-funded music in the park series is perfect for foolish dance.  So soon a group of kids joined the four, and were doing floss dance and fortnite moves to the music.  A group of friends behind us were cheering the kids on, and lost their minds when two of the kids started doing the worm.  The cheering was ironic, but not fully so--you could tell they were legitimately supportive and the irony and beer just let them loudly express it.

Midway through the set a trio of teen girls joined the dance floor, and stayed in a tight circle performing half a floss move or half the macarena before laughing and looking around--just pure self-consciousness, making goofy movements cos it's better than having yr hands in yr pockets.

But the set just kept getting better, the drums louder and the synths bright and dancey.  With the sun setting, and the park a nice dark glow, the iriony in the dance started to break down.  One of the girls, in a blue linen op shop dress, danced her way towards the idiosyncratic dancing leader of the awkward dudes and started lightly mirroring his moves until soon they were dancing to each other and with each other.  Her friends kept calling out from the back and she would turn around and laugh and yell and get back to dancing until her friends came and joined in.  So now there was just a group of strangers all doing the same bizarre dance with little self-consciousness, no irony, and full joy.

By the last song, the stage was packed with friends playing Sax, Trumpet, drums, and guitar, and the asphalt was now packed with dancers. When they finish, everyone screams for an encore, and so Lake South played a 2 minute rave up as everyone bounced up and down.

When it was over, a man in a suit came out to thank us for coming out, and to thank the Wellington City Council for putting it on, and to thank Lake South for playing and then said, "That's the music show, please stay for the light show" and the trees around us all lit up bright neon.

We stayed in the park a bit after, walking through fragrant garden, stopping at each bush to smell the flowers, trying to politely avoid the teen couples looking sheepish in the alcove park benches, and then Angelica and I wove our way out of the garden and into the cemetary path leading back into the city, talking about how we both think Bigfoot exists, but the reason for its existence is of course up for debate.  It made sense to me that there might be some overlay of space and time and dimension, where big foots are from some other time and place and accidentally showing up in our own--like a strange shape in your periphery.  Then Angelica mentioned that some people think Bigfoots can fly, which was just too much for me.  I am sincerely touched by the love people have for Bigfoots, though, especially how there has always been a group writing horny romances about them.  Angelica agreed it was weird and sweet.

Bigfoot doesn't have anything to do with the show or wellington or dancing.  I'm just wanting to give an accurate log of my day and its details, and frank Bigfoot talk is how it ended.

Thank you.

◊mood{happy}

◊|end|

◊(title "Nice day"
  "9 December 2019")

For dinner we ate kimchi and rice and watched documentaries about the CIA.  Angelica bought me beer (new ne wnew coproration beer, my favorite) and the kimchi was fresh, and so I'd take a big bite of kimchi and a swig of beer, and there'd be two types of delicious savory bubbles all refreshing in my mouth.

I finished Ealanor Davis' book ◊book{You &  A Bike &  A Road} after dinner, enraptured and crossleggd at our coffeetable.  It is such a beautiful book and I recommend it whole heartedly.  The journal comics are always so inspiring, as it seems often that life catches them by surprise too...you don't know the narrative that your journal would dtake, it's just a daily jot that in hindisight has the shape and appeal of a book.  Of course there's editing, and an authorial eye in what you focus on, but you don't have to know the ending to write a good journal.  You just have to to know to write.

We walked up to the community garden after to drop off our compost.  It's summer enough now for me to go barefoot, which feels special in New Zealand, like the sidewalks are just warm enough.  It's 8m by this time, twilight and mostly empty on the streets.  To get to the garden you first pass through the lawn bowling club.  There's two bowling courts(?), big squares of grass kept short, with staged seating on one side and bathrooms and supplies on the other.  Behind the staged seats is pure bush, and the wildness with the square of green is disorienting, like you've encountered the most banal ancient structure.  We were not alone here, though.  An old man sat in a lawnchair on the porch of the clubhouse, lost in thought, before two young girls (maybe granddaughters?) ran up to him with happy yelly chatter.

A small gravel path connects the bowling club to the community garden, a place poetically called Innermost.  Half of the garden lots seem to be forgotten, or the gardneer is doing an intense form of biodynamics and so Innermost is almost as wild as the bush that surrounds it.  It makes the birds here so happy, their song loud and diverse as we walked the path to the compost bin.

When I moved to New Zealand, I found the birdsong striking, but it was the din of it.  This interesting new mass of sound.  Now I am struck by the specific personalities in the din.  I've grown more familiar with each melody, and can tell the birds apart.  I just don't know their mood, or needs, or context.  I love this inability to know.  The birds last night all had a lot of things going on in their life, and we humans really didn't matter.  We were a backdrop to their busy days.  The calls and responses feel important, like trilling out a business call.

Flowers are blooming all over the neighborhood, pushing through fences and hanging over the sidewalk.  We stopped to smell each one and, if it smelled nice, Angelica would ask the plant permission then lightly snip a bud.  As we reached the house, she had a handful, a potpourri to pour into the tub later when she took her bath.

I set up in the ofice for the evening to listen to my friend's new album and write out all the images and memories the songs were conjuring up.  He's releasing it on the odl web on Friday, and wants to make a Song Island for it to put out in the chorus before.  I feel so excited and stressed to make something that honours the talent in the songs.

I'm finding myself with an existential tic as of late, where I keep trying to see the big life picture and present moment simultaneously, and getting myself cross-eyed.  Maybe it's my age, or location, or the big changes that I've gone through in the last couple years (marriage, New Zealand, coding) -- but everything I do or attempt to do has this private inner weight to it, like it needs to fit into an already-established narrative.  I will be eating an orange, and this annoying faux-intellectual voice will say, "And it was at this point in his life that Zach decided to eat an orange.  This followed a motif in his life where he regularly ate food."  Or, more seriously, I will spend an evening coding and think "perhaps this is what you do now, the big shift.  Silently leaving these artifacts, instead of making entertainment yourself.  Or perhaps these will be seen as your entertainment."  It's a dumb way to think, treating yr life like a puzzlebox.  Or rather, it feels like a movie that you were told has some big twist, and so as you watch it you can't stop analyzing each scene, wondering if you can decipher the twist before it appears.  Except, you got the title wrong.  It's a wholly different film, one not known for any clever twist...just beautiful surroundings and a killer soundtrack and long stretches of meditative moments in a peaceful, plotless dream.  It's best when I enjoy it scene by scene.

◊mood{peaceful}

◊|end|

◊(title "An Online Air"
"17 November 2019")

It rained a lot in my hometown, with enough intensity and frequency to create a local "mood".  Olympia broods well-- we know how to be depressed, but interesting, to live in a cycle of lethargic, artistc yearning broken by bursts of joyful release.  You could trace so much of this local pattern to the rain, how it forced us inside and away from another but near records and books, and how the black mold in our badly made apartments caused a vague and sluggish sickness that justified skipping out on plans.  And how, when the sky was blue and the spirits were up, we knew to make the most of the moment, a partying already tinged with melancholy cos you knew the blue sky would not last.

Here in Wellington, it's the weather that makes this place feel like home.  The days when the wind and rain keep me inside,  and I'm kinda bummed but mostly comfy, feeling an instinctual peace with my blanket, good book, and cancelled plans.  It's also prolly why I like the people so much here, they have a creativity and sadness that feels familiar.

I wonder when the web will be seen as a place we are surrounded by, like the trees and water that surrounded me growing up, and whose patterns can affect our behavior in a similar way to the weather patterns of our youth.  Right now I feel this to be true, but I feel immediately silly, or corny, for typing it.  I think in the future, this will feel less silly to say.

This morning I was talking to Angelica about the work I did on Song Islands last night, and as I started to ask her a question, I found myself tongue-tied.  I'd said, "Do you think, maybe..." and then I couldn't get the next word out, got anxious about how long I was taking to finish the sentence, and had to avert my eyes to try to get the rest of the words out.  This has been happening more lately, this struggling for words triggering an anxiety over my slow speech, that ends with me frustrated and tongue-tied.

The sentence I was trying to say to Angelica was, "Do you think you could help me with some design choices for Song Islands later, because I'm feeling aesthetically stuck."   The sentence is simple in word and grammar, it's subject and emotion are clear, but when I got to the word 'you' my brain pinged "It's not her job to help you, recognize this" and then "Why are you asking her for design help, what assumptions are being made?" and so i switched out the you for a maybe, hoping it'd give me time to adjust the sentence.

These mental pings were ridiculous, and I knew they were.  Angelica is my wife, and we have a relationship built on mutual support.  We love helping each other, and collaborate often.  I fell in love with her art before I fell in love with her, so of course I want her artistic sense on our shared project.  The doubting thoughts didn't feel mine, rather like things that strangers might think if they'd overheard. And I realized I was imagining the comments to my sentence as I spoke it.  I felt an emotion that sparked an impromptu sentence, and now I was editing that sentence in real time based on its imagined, ever-changing, replies thread.  Of course I would get tongue-tied.

This particular exchange might be a special Zach Mandeville anxiety, but I think it's general shape is one shared by a lot of people.  If we spend so much of our day in spaces built on antagonistic feedback, wouldn't it make sense that these spaces affect our behavior?  What sort of ambience does the presence of your phone or computer give to the room you're in, is it a quiet influence like the rain pattering against your windowsill?

We were listening to the Struggle Session podcast yesterday where they gave their first impressions of the game Death Stranding, and it seemed like they were unable to describe their feelings without framing it in opposition of someone else.  They'd say the game was artistic and strange, and then be upset that art gamers aren't celebrating it more.  And they'd say that it was beautiful to see Kojima able to fully explore his vision, and how dumb is it that people give him a hard time for it.  But I didn't know any art gamers were not celebrating it, or that Kojima fans would be mad that he followed his vision.  They were framing their view as an underdog opinion seemingly on reflex.  At one point, one of the hosts remarked how Norman Reedus has to soothe a baby as one of the main mechanics in the game, and then said, "And I keep waiting for someone on twitter whose never played the game to have a picture of norman reedus and say 'ugh i hate all these masculine white men video games'".  This tweet does not exist, it is one they imagined and are now waiting to manifest.  Or rather, they had a direct experience with a game, and the way they could describe it is by imagining how an uknown person would make a foolish tweet about it, and the snide comment they'd make in their head as a reply.

This afternoon, we went to the Wellington Zine Fest and it was so lovely, and inspiring, and well-run.  We picked up some awesome zines and met incredible people and it was a through and through good time.  But I also had this lingering unsettled feeling from it.  They had a couple quiet areas that were filled with zines the festival attendees had donated.  I took a few quiet breaks and read through a batch of the personal zines and diary zines (my fave).  So many of them had this tic in the writing, where their feelings were expressed in a statement so edited for precision that it felt like legalese.  One zine had this sentence about themselves that they'd footnoted, with the footnote saying, "and before you get mad at me for saying this, or try to tell me X, know that Y and Z, etc."  They were opening their heart out in a diary, pre-emptively arguing with the imagined comments, and writing their replies in the diary too.

I'm not bringing these up to prove some point.  I have no thesis, this is just a diary of things that happened recently.  But these anecdotes feel connected to me--they feel unpleasant, but familiar.  Each one is an expression that, before it could be articulated, had to travel through an imagined online space.  They are moments connected by a shared digital upbringing.

◊mood{pensive}

◊|end|

◊(title "Midsommar is really good"
"Monday, 11 November 2019")

Angelica and I went and saw Midsommar tonight and hoo boy what a good movie.  If you are in the Wellington area for the next couple weeks, I say get yourself down to Lighthouse Cinema on Wigan St.  And let me tell you, after you see it you'll be the one ◊em{wigan}, wigging out that is!

◊note{
  This was a shoehorned joke, I am sorry.  Every day I walk down Wigan street on my way to work, and every day I say to myself "Wigan wigout".  And this phrase?  It brings me joy.  But today as we walked to the theature, I told Angelica "they should have a street festival called Wigan Wigout" then this evening I write to you with basically the same joke, the joke I've now told three times in three sentences.  It's a fixation more than a joke at this point.}

Anyway, the movie is real good.  It's pretty, and weirdly calming?  Everything is paced so intentionally that there's not really jump scares, just repeated opportunities to accept the inevitable.  And there's a visceral artistic intent in it, as if themes and structure came accidentally.  Like...it felt both a pastiche/genre-tribute to folk horror and an honest ritual being enacted for the filmmaker's benefit more than ours.  When we got home, I looked more into the making and interviews with the filmmaker Ari Aster, and it turns out the movie was a contract job.  He was asked to make a horror movie like ◊movie{Hostel}, but based on Midsummer. However, he started making the movie right after going through a breakup, and shot the movie quickly.  So instead of getting a Swedish ◊movie{Hostel}, we get this weird, wavering horror.

After the movie, I went to the water spigot to refill a bottle Angelica had bought.  The spigot had a sign saying "tap turns off automatically" and on the first press, it filled the bottle about 2/3 the way.  So I drank a bit and pressed again, and it filled it just a drop more than before, back to 2/3.  So I pressed the tap again, and it filled it to the brim and kept going, overflowing over the bottle into the grates below, and  I went "oh no!" and  tried to pull the tap to get it to stop--more in a symbolic gesture than earnest attemp.  As I pulled my water bottle out and turned to leave, I realized there was a woman waiting behind me the entire time to get a glass, and she had ◊em{such} a disgusted look on her face0-- though it wasn't a judgemental look (which might've been better), it was like surprise disgust, like when you smell a fart.

But it was my actions...my actions were the fart.

Not sure why I'm telling you this, just felt fitting for here: my secret public diary of my secret public shames(+ movie reviews).  

◊mood{upbeat!}

◊|end|

◊(title "Dev Diary Livejournal"
        "Monday 11 November 2019")

Hello friends! I meant to write you yesterday, but I got legswept by computer code. I've been working on a zine| tool | site | dream called ◊cglink[#:href "projects/song-islands"]{Song Islands}. I can speak of the project in one way, and it's this lofty dream that combines everything I love about art, the past, the future, and the internet. I can speak of it another way and it's basic site that lets you send a song to friends. Yesterday I felt confident, and was describing it to myself both ways at once...a lofty vision of the future that is so basic in my mind that I can realize it easily. I wanted to add a new element to the song islands, and would code it quick before setting out to write my evenings thoughts. Then I somehow broke my whole program, loading a blank page with every refresh, with the computer console returning the cryptic error message: "[cannot continue]...stop() is not a function". So I spent the next couple of hours trying to fix this error, or at least understand it, stayed up far too late, and went to bed with a broken site and no diary shared.

I did not realize how lonely coding would be. A couple years ago, I felt a pull towards the computer, a wish to express myself through it, using a writing style that felt myserious and runic. And it still is! It turns out I love coding a lot! But I wasn't anticipating how rough this midpoint would feel-- where I am not practiced enough to articulate my feelings well in it, but have become rusty in all my old methods too.

The loneliness comes, I think, from not feeling like I can connect to y'all through it yet. I get such stress when talking about code here, an unfounded worry about the phantom expectations of my unknown website audience. If you, dear reader, are not interested in computers than I worry these words will be boring at best, and alienating at worst. And if you are into computers, then I worry all my words will be embarassingly wrong, and mispelled. To try to connect through the coded objects is tough, as so much of the work that made it meaningful for me is private. It's being able to manifest an object that didn't exist before and not knowing, until it appeared, whether it would ever be possible for me to do it.

This sounds mighty, but at my current ability, all the things i'm gobsmacked at being able to manifest are basic and small. For example, The next iteration of song islands, currently, is a blank page and an error message that sounds like a sports ad("Gatorade: there is no stop"). I spent hours experimenting with my code to make that error message disappear, poring through documentation trying to uncover where ◊code{stop()} tried to, and failed, to exist. If I'm one day successful with this I'll be able to have...a web page, with a peach background, that shows a list of song titles.

So much of coding feels like climbing up a mountain to bring back a pint of milk, all showing it to yr friends like "can you believe it? I brought this back with my two hands and two feet, that's it!" and they're politely nodding, wondering why you're so out of breath and how a pint of milk could make someone so sweaty.

◊|pause|

I write all this as if it's new, some undiscovered feeling whose shape I'm beginning to make out. But this loneliness is so common, and so shared! As I write this, I realize the core of this post is that there's big dreams in my mind, and a frustration that I can't make them real quickly enough, or that the thing that makes them special to me won't be understood by you. But that's the loneliness in all expression, the heart breaking as it opens fully, hoping you can recognize its shape. In other words, this is all a positive: I've just discovered a new way to break my heart, and it's a program you can run in yr web browser.

Tonight, I had a spark of inspiration, read through the chat logs again for the coding framework I'm using, and found the answer I'd missed last night. There's a notion of javascript called 'promises', which is a function that sets out to retrieve some data, but sits as a in-between placeholder until the true value arrives. I wanted to populate a page with track lists, and so used a promise to do so, which the compiler would not accept. The error message was the compiler's attempt to tell me, "I can't fill this page with promises, I need something tangible". And even it's way of speaking made no sense, I totally understand what it was going through.

It's not sharable yet, but I got the peach page of tracks to appear and it feels fantaaaaaastic. But I didn't need to tell you how it felt, I'm sure you get it.

◊|end|

◊(title "Patterns"
"Monday, 27 July 2019")

I am writing tonight from a space far lovelier than all of Tukwila.  We are staying over at our friends K and C, at their farmhouse outside of Tumwater.  There's a window in front of me, and when I look up I see the blue green outline of pine trees.  There's a window to the left of me, and when I look out I see pine trees lit up full by the light of the bedroom next door.  Whether in shadow or crisp detail I got pine trees in all eyes and I am so happy.

K and C are my age, but have their shit together so calmly and confidently that their home feels like a grandparent's house.  Chickens and goats are sleeping outside, their baby is sleeping next door, they are softly talking in the kitchen as they do their evening chores.  All the food is from their farm or a farm nearby, and tonight they served us a Georgian dish called Kachapuri (made in the Adjarian style), which is essentially a dilly and eggy cheese dip served inside a boat of bread.  We are in a warm office with a pull out couch, bust just in case it gets cold in the night, we were told there's a closet filled with blankets right outside our door.

You do not get a closet of blankets by accident.  Blankets are not just given away.  At some point you have to point to a space and say "No one should be cold, we will fill this space with tools for warmth."  It is a good type of person who is young and has a closet of blankets.

Tonight was so nice, today was a grand meh.  We set out from our hotel at noon to catch the bus to Olympia.  The city is only 60 miles away, but the trip took us 5.5 hours and the majority of it was spent in and around the Tacoma mall.  I'm setting this up like it's an interesting story, but it's really not: I had to go pee, this state has no public bathrooms, and I made us miss the bus because I had to walk to a best buy and back to pee.  The bathroom sitch has been a culture shock for me, coming from New Zealand.  In NZ, I _always_ know I can go pee, and it seems to come from a larger trust and caring from the people there.  At the Tacoma park and ride there was a building clearly holding bathrooms inside, but every door was locked and accessible only to bus drivers.  So the park and ride designers knew  this area would be one in which folks needed to pee, they wanted to make sure only the employees could.

On the bright side we got to hang out in the mall for a bit, and watch all the beautiful people trying their best to be better than this day.  That's the other culture shock for me so far--is that everyone seems to be struggling with a headache all the time.  It's like the whole country is a pair of fingers pinching the bridge of its nose and sighing.  Which just makes this evening at a friends all the sweeter.  I'm chatting with my friend R back in NY while I listen to Geotic and the house settles into quiet around me. I'm trying this new thing where I give friends initials instad of their names, so I feel better writing about them.  I don't know why i'm shy, since i'm always saying nice things, but it feels polite.

this is a true blue diary entry.  no real shape, no real purpose, a couple of good feelings and a flash of annoyance that settles back into good feelings as the writing calms the heart, and then bed.


I am writing tonight from a space far lovelier than all of Tukwila.  We are staying over at our friends K and C, at their farmhouse outside of Tumwater.  There's a window in front of me, and when I look up I see the blue green outline of pine trees.  There's a window to the left of me, and when I look out I see pine trees lit up full by the light of the bedroom next door.  Whether in shadow or crisp detail I got pine trees in all eyes and I am so happy.

K and C are my age, but have their shit together so calmly and confidently that their home feels like a grandparent's house.  Chickens and goats are sleeping outside, their baby is sleeping next door, they are softly talking in the kitchen as they do their evening chores.  All the food is from their farm or a farm nearby, and tonight they served us a Georgian dish called Kachapuri (made in the Adjarian style), which is essentially a dilly and eggy cheese dip served inside a boat of bread.  We are in a warm office with a pull out couch, bust just in case it gets cold in the night, we were told there's a closet filled with blankets right outside our door.

You do not get a closet of blankets by accident.  Blankets are not just given away.  At some point you have to point to a space and say "No one should be cold, we will fill this space with tools for warmth."  It is a good type of person who is young and has a closet of blankets.

Tonight was so nice, today was a grand meh.  We set out from our hotel at noon to catch the bus to Olympia.  The city is only 60 miles away, but the trip took us 5.5 hours and the majority of it was spent in and around the Tacoma mall.  I'm setting this up like it's an interesting story, but it's really not: I had to go pee, this state has no public bathrooms, and I made us miss the bus because I had to walk to a best buy and back to pee.  The bathroom sitch has been a culture shock for me, coming from New Zealand.  In NZ, I _always_ know I can go pee, and it seems to come from a larger trust and caring from the people there.  At the Tacoma park and ride there was a building clearly holding bathrooms inside, but every door was locked and accessible only to bus drivers.  So the park and ride designers knew  this area would be one in which folks needed to pee, they wanted to make sure only the employees could.

On the bright side we got to hang out in the mall for a bit, and watch all the beautiful people trying their best to be better than this day.  That's the other culture shock for me so far--is that everyone seems to be struggling with a headache all the time.  It's like the whole country is a pair of fingers pinching the bridge of its nose and sighing.  Which just makes this evening at a friends all the sweeter.  I'm chatting with my friend R back in NY while I listen to Geotic and the house settles into quiet around me. I'm trying this new thing where I give friends initials instad of their names, so I feel better writing about them.  I don't know why i'm shy, since i'm always saying nice things, but it feels polite.

this is a true blue diary entry.  no real shape, no real purpose, a couple of good feelings and a flash of annoyance that settles back into good feelings as the writing calms the heart, and then bed.

◊mood{happy}

◊|end|

<!-- 
     Local Variables:
     eval: (writeroom-mode)
     eval: (spacemacs/toggle-visual-line-navigation)
     End:
     End: -->
