#lang pollen

◊define-meta[subtitle]{welcome to song islands}
◊define-meta[background]{#F6FFFE}
◊define-meta[color]{#FF41B4}
◊define-meta[accent-color]{#8484d9}

◊h1{What Is Song Islands?}

Song Islands is a single song mixtape.
It is a love letter to mixtapes.

It is a single song mp3 blog.
It is a love letter to mp3 blogs.

Song Islands is an audio postcard addressed to you...maybe even a a love letter to you? (I may not know you, and don't want to be forward or insincere.)

It definitely ◊em{is} a love letter to ◊em{you} (you know who you are).

And it's a love letter to my friends and my old friends and the people I can't write to anymore.

Song Islands is intended for artists, in whatever medium, at whatever level, they make their art.  It is for the people who offer more than they take.

It is a love letter to creativity and art.
It is a tool for writing your own love letters, and sharing your own art.

Song Islands is a dat zine and a tool for making dat zines.

It's a CD-RW scrawled with a magic erasable marker.

And of course it is a love letter to zines, dat zines, cassettes, CD-R's, photo copiers, instruction manuals, held out hands, comments in source code, and the embarassing earnestness of away messages, gamefaq dedications, and posts about your personal life that you wrote in record label message boards.

More than anything, though, Song Islands is an altar.

There is a future I want manifested, where technology augments communities and supports local scenes, where the internet is human and sweet and empowering.

I would like this world to rise, and so I wrote this code up and filled it with intentions.  This is a small offering-- four stones and a purple candle on a modest white cloth, but I am excited for how this altar will grow.

Song Islands is a song, a picture, and some words.  It is as simple and infinite as that.
