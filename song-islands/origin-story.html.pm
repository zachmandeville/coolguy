#lang pollen

◊define-meta[background]{#F6FFFE}
◊define-meta[color]{#8644c6}
◊define-meta[accent-color]{rgb(206, 103, 165)}

◊h1{Origin Story}

Song Islands initially came as a reaction to streaming platforms, and the way tech people talked of alternatives.

A signifying moment came last year, when Soundcloud came close to shutting down, and a conversation sparked up on ◊techterm{scuttlebutt} about making a better alternative. A developer joined the conversation and was all fired up about using this new protocol to fix every music-platform woe.  Their strategy to do this, though, was a Silicon Valley strategy: make a flashy impact that gets investment, then use investment to create a service with broad appeal whose prime motivation is user-count, and make sure it follows all the legal requirements of the record industry while also getting you paid....but do it this time with decentralized tech.

It is a misguided, and common strategy in tech: fix a problem by creating a new version of it. This comes often with a myopic viewpoint, where the root of the real-world problem is due to some element in a tech stack, and the solution is to use different tech you're more excited about.  This strategy usually comes with condescension and ingrained superiority: "Soundcloud is bad, it is our duty to replace it. So now, how do convince the millions of people using it that our thing is better?  Keeping in mind, of course, that most of them are idiots, so the values we see in this will need to be dumbed down so ◊em{they} can get it. And we'll have to make it not scary for investors.  But man, if we can do this, we'll change the world!"

So they worked hard on this for about a month, making compromises based on critiques they imagined investors could make, and in the end came up with a service that let users download thousands of public domain tracks, with an 'seo-friendly' app name that unintentionally sounded 100% like it was for swapping childporn.  They burnt out as their splash page went up to zero excitement and unanimous confusion around the name 'lolashare' and the project was abandoned.

Around the same time, my friend ◊peep{@cblgh} shared( also on ◊techterm{scuttlebutt}), a project they'd started called datradio.  This was a ◊techterm{dat} site you reached through ◊techterm{beaker browser}. He was using it to share loops and beats he'd made.  There wasn't a large investment plan yet, or a roadmap for implementation, it was a dream and an experiment and an excuse to share some songs. 

His work thrilled me, and inspired me to think on what an alternative to the streaming platforms might look like if you embraced modern technology but not modern tech culture.

I started to write up my thoughts into a zine, one that kept growing and became harder and slower to write.  One problem is that, as I was writing all this, I was also learning how to code.  So I kept getting a pang of guilt with each new page-- this feeling that I was telling others what to do when I could try to just do it.

And so the writing is still forming itself into the zine ◊a[#:href "https://coolguy.website/writing/music-visions" #:target "_blank" #:rel "noopener noreferrer"]{"Music Visions"}, while the code is forming itself into these Song Islands.  Music Visions is the lofty dreams and bold accusations and idealized cartography.  Song Islands represents the best I can do, at this moment, to realize those ideals.

The mission of Music Vision that I am trying to manifest in Song Islands is this:
- Don't make something for millions, make something for one other person.
- Create patterns instead of platforms.  Give people a tool to use instead of a site to log into.
- Make code that feels closer to sharpies and scissors than google docs.
- Make something for creators, instead of consumers.
- Make something that supports local scenes, prioritizes small towns over major industry.
- Make something beautiful and filled with human passion. Not the passion you put on a resume, the passion that makes you blush.
- Make something that, like the best book or set or painting or movie, will help people get laid.

Both Music Visions and Song Islands are unfinished, but growing.   To be honest, I am fighting a real strong urge to either not share until I feel it's ready, or to just burn it all now.  I know neither are what I ◊em{really} want, so I'm giving these early sketches to you now, putting a markder down at this exact time and place, so I can keep on going.
