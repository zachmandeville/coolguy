#lang pollen
◊define-meta[background]{#F6FFFE}
◊define-meta[color]{#3ddc97}
◊define-meta[accent-color]{#9f5eb7}


◊h1{Practical Deets}

Song Islands is a song, a picture, and some words, whatver song, picture, and words you'd like.


It looks like this...

◊image["title-page.png" "title page of a song island"]{Huge Words in Thin Font that say 'Autumn Augment, Lavender Thrall, enter'.  It's a song title, a band, and an invitation.  Behind is a repeating pattern of lavender sprits}

When you enter, the chosen song starts playing with a full screen image to zone out on...

◊image["main.png" "screenshot of main page of song island"]{same lavender sprig background-- lavender bar up top with small pixel icons showing musical notes, a piece of paper, a cursor, and a happy cat with headphones.  In the top right is an edit pencil and a button to make a copy. Two boxes are placed in bottom corner, one attributing the song playing, one attributing the picture you see.}

or you can click on the charms up top to explore different views.

There's a nice audio player...

◊image["audio.png" "screenshot of audio player"]{same background, but with what looks like a small lavender mp3 player in the center.  It has control buttons in same hand-drawn pixel art.}

And a set of words, that can be as long as you need...

◊image["words.png" "screenshot of words box"]{music player replaced by large window with a semi-transparent green gradience, on top of which are typed out words.  Looks like a seapunk version of an old textedit window.}

There's also links for more info on the band or the artist...

◊image["links.png" "screenshot of links box"]{solid lavender rectangle with chunky green buttons indicating different websites to visit, organized by song and artist.}

◊h2{Reaching Song Islands}

You can visit a song island template here:
◊a[#:href "dat://song-island-style.solarpunk.cool"]{dat://song-island-style.solarpunk.cool}
and you can view the code for the starter template here:
◊a[#:href "https://gitlab.com/dat-zines/dat-song-island-style"]{https://gitlab.com/dat-zines/dat-song-island-style}

Song Islands is built for the chorus: a world of radical, empowering, decentralized, and solarpunk tech.  It is not on the web, and will require a new browser to visit.

This browser is called ◊techterm{Beaker Browser}.  It is free, and open source, and can be downloaded here: ◊a[#:href "https://beakerbrowser.com"]{https://beakerbrowser.com}

Dat sites (like song islands) work different from the web in that you can host the site directly from your computer, and your friends can help host from their computers.  This means you do not have to rely on any middle service (like godaddy or squarespace) to have a site.  It also means you can build a music network entirely for your friends, supported by your friends.

◊h2{DIY}

Another special property is that any dat site can be copied, to act as a starting template for your own site.  

Song Islands embraces these properties.  They are meant to be shared and enjoyed among friends, and as a source of inspiration(and toolset) to create your own.

On each song island is a "make copy" button in the top right.

◊image["make-a-copy.png" "make a copy!"]{green button on lavender background that exclaims "make a copy!"}

This will prompt you to copy all the code to your computer.  This will open as a new tab in beaker browser.

◊image["copy-dialog.png" "dialog box for copying"]{system dialog box confirming you want to make a copy of this site.}

In your own song island, you can now click the edit pencil in the top right, bringing you to an editing interface where you can adjust the song, words, picture, and aesthetic of the site to make it your own.

◊image["edit-page.png" "screenshot of edit page for song"]{full lavender background of the back office.  on the left is a sidebar with a mini music player, and links to each section to edit.  Currently showing the song section, with chunky green buttons to edit song and add and remove links. Looks slightly like mid-90's educational software, but dayglo.}

◊h2{Enjoy}

Song Islands is not a platform or a business.  It's a technique, a pattern, and a toolset-- closer to scissors and sharpies than Spotify or Soundcloud.

It was coded by me, Zach Mandeville, with all the assets drawn and coded by Angelica Blevins.  We collaborated on the aesthetic and design.

I hope you enjoy it, and please share any song island you'd like with me!

