#lang pollen
◊define-meta[title]{index}
◊define-meta[background]{#F6FFFE}
◊define-meta[color]{#FF41B4}
◊define-meta[accent-color]{#8484d9}

◊h1[#:class "mb0"]{Song Islands}
◊quick-link[#:class "quick-link" #:href "dat://song-island-style.solarpunk.cool"]{dat://song-island-style.solarpunk.cool}
◊toc[]{
◊toc-link[#:title "What Is Song Islands?" #:href "introduction.html"]{some context and intention setting.}
◊toc-link[#:title "Practical Deets" #:href "the-deets.html"]{How to find, use, and fall in love(?) with song islands}
◊toc-link[#:title "Origin Story" #:href "origin-story.html"]{Why I made all this.}
◊toc-link[#:title "Music Visions" #:href "https://coolguy.website/writing/music-visions"]{An expanding zine about how radical technologists could help music communities (plus love stories).}}
