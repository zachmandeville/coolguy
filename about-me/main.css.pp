#lang pollen

◊(define color-1 "lavender")
◊(define color-2 "#EAEAF6")
◊(define color-3 "#EEEEF2")
◊(define color-4 "#F0FAE6")
◊(define color-5 "#c2aed6")
◊(define title_color "#433633")
◊(define glyph_color "#946e83")

body#about-me {
  background: linear-gradient(to top right, ◊|color-5|, ◊|color-4|,◊|color-3|,◊|color-2|,◊|color-1|);
  font-family: var(--primary_text);
}

#about-me h1 {
  margin-top: 2em;
  font-family: var(--title-text);
  letter-spacing: 3px;
  font-size: 2em;
  color: ◊|title_color|;
}

#about-me em {
  font-weight: 200;
  font-size: 1.1em;
}

#about-me a:after{
  color: ◊|glyph_color|;
}

#about-me article {
  width: 70%;
  margin: auto;
  margin-bottom: 3em;
}
#about-me article p {
  line-height: 1.4;
}
@media (max-width: 667px) {
  #about-me article {
    width: 90%;
    font-size: 22px;
  }
}


