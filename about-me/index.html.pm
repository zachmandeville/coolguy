#lang pollen
◊h1{About Me}

My name is Zach Mandeville.

I am the webmaster for coolguy.website.

I have had two significant paranormal experiences.

The first was when I was 7, in the living room of my house in Nenana, Alaska.

The second was in Derry, Ireland at a tragic family manor turned youth hostel.

My favorite food is veggie tostadas.

I was born in 1985 in Delta Junction, Alaska at 10:30pm. This means, according to the natal chart drawn up for me at ◊link[#:href "https://cafeastrology.com/articles/howtoobtainchart.html"]{cafeastrology.com}, I am an Aries, with a Capricorn moon, and Scorpio rising.  Which, if you knew me or read my chart, you'd be like ◊thought{yep!}

◊aside{For those interested, I've included my natal chart to this about-me, and you can view it ◊link[#:href "../files/natal-chart.pdf"]{here}.}

I say I was born in Delta Junction, but that's a slight lie.  That was the town my family was living in, but it was too small to have an obgyn and so my actual delivery was 95 miles north, the nearest hospital, in Fairbanks.

Delta Junction ◊em{feels} like my birthplace and the name is more fun to say (with the radio static sound in the center of Junction), and so I lie about my birth to fit a better narrative— even though both the narrative and lie are boring.

This tendency towards a meager embellishment for a small aesthetic gain is a “crutch" of mine.  I put that in quotes only cos I'm not confident I'm using the word correctly.

This lie does not affect my natal chart, though.  I am Aries, Capricorn, Scorpion whether no matter how pedantic or romantic about birthplaces you be.

I am allergic to cats and eggplant (slightly).  The eggplant allergy might just be cos my brother was allergic to them, and I look up to him, and so as a kid I might’ve aped his allergy and psychosomatically manifested it.

(They make my tongue itchy.)

I’m 6’2 with brown hair and blue eyes and my weight fluctuates more than I like, but always within “chubby”.  It’s a pleasant-enough chubby, though, and I’ve reached happy weights and know they are in my grasp.  My weight always goes up when my mood goes down, and so the desire to lose weight is really a subtle desire to be reliably happy.

I identify as male, the gender assigned to me at birth, but am happiest and wholest after embracing my internal androgny.

My true hometown is a place in between Tumwater and Olympia, Washington— named Funwater.

I practice magic (the earth kind, not the stage kind) and am a gentle quaker.  Belief is not something I could summarize easily, rather I hope it to be a backlight to all  my words and actions.  But a short hint: I believe magic is as real as sound and earth, and for the world to be animist and a subtly shifting expression of love; like a melodic pattern that transforms slightly with each refrain.

When I was a kid, I had a lot of ear infections.  I also once drank so much egg nog that my body rebelled with a three-week stomach bug that nearly hospitalized me.

I had a friend back home that I had a crush on, but it was a weightless sort of  crush.  I didn’t want to be with her, I could only picture friendship, but I found her overwhelmingly lovely.

She was telling me once of an X-Men comic she was reading and liked cos it was kinda queer and written by Grant Morrison.  And there was this gentle strange character in it who, when he got his power, described it as “a flower blooming inside my head”.  And when she told me this her voice got softer around ’a flower bloomed…’ like she was trying to give the words respectful whitespace.

I read the comic later and it ends so badly, the character is just fucking Magneto and it makes no sense.  But that one passage is still incredible, and I find myself using that phrase to describe any epiphanies and discoveries I’ve had.

In the same way you always describe a calm weekend as “restful” or a confusing film as “either the best or worst thing I’ve seen”, I describe so many moments as “flowers blooming inside my head” and I wonder if a head can really have that many flowers.

BUT!  There was a moment about two years back, when I started to write this site inside the command line using vim, and it def. felt like a flower blossoming inside my head.  It fully did my head in.

One day I’ma be able to express all my heartbursting feelings around computers and it will be the romantic, moon-full rush that I feel, and not just a mist of malformed words. Right now, I feel tongue-tied and it’s frustrating.

I feel a slightly similar, though much milder, heartrush towards the cheerleader comedy ◊movie{Fired Up}.


The naming of "favorites" is kinda silly, in that it seems to not allow any sort of growth.  You name your favorite movie, and then all movies after just don't match?  Or your tastes are locked in to whatever you first deemed favorite?  But I still think the feeling of favorites is important, even if the word isn't right.

There are aspects of me that I want to keep strong forever. Ideals and understandings that sprung up from some piece of art, some combination of senses.   And I think it is good to have a set of favorites that summon these understandings like a memory summoned from a scent, or a spirit from a sigil.

A friend back home called these "heartstones", and I think she used a far better word than "favorite".

My favorite book is ◊book{My Antonia}, by Willa Cather.

My favorite plant, color, and smell is lavender.

My favorite film is ◊movie{Tully}.

My favorite tea is Irish Breakfast.

I once won a region-free DVD player, and a collection of obscure horror DVD's, by shoving 34 marshmallows into mymouth.

My first job was as the freezer guy at the Grocery Outlet in Olympia.

I have two tattoos, one is Carl Sagan, taking up my whole upper right arm.  The other is the aeroplane gramophone from Neutral Milk Hotel's ◊em{In the Aeroplane Over the Sea}.

My least favorite chore is either washing silverware or cleaning the tub.  I love doing the dishes, and I'm fine with a bathroom, but those parts I just lose all my energy for and feels so arduous.

I think the author Lois Lowry (author of ◊book{The Giver}) is underrated.  She's prolly super-well-respected and people like her, but I don't think people like her ◊em{enough}.

My biggest fear is driving and either having someone being running down the road towards my car, or for me to turn left and see someone running alongside the car looking in.

My most recurring stress dream is to be in a house with a lot of doors and windows, and watching a threatening person trying to get in.  There's always a moment in the dream when they come to some door and it is locked, and I am seeing them from inside just as a realization flashes on their face and they run to some other side of the house, seemingly to get to the door I forgot was unlocked.

Shit makes me feel all tense just writing it.

I love the words spring, destroy, pleasant, galore, fancy, and be.

I cannnnot STAND the word nibble.

I also cannot stand when the word quietly is used to describe the quality in which someone did something (instead of the volume in which they did it).  I think it's because quietly just feels like a synonym for "smugly", but like the writer is projecting their own smugness onto the writee.

When I was last in Olympia, I went shopping at Pyschic Sister (the best clothing store), and found this appealing mint-green work shirt for only 10 dollars.  The shirt felt heavier than usual and had thick black buttons and velcro around the sleeves.  When I looked up the brand, it turned out to be a welder's supply and the shirt was so heavy because it was ◊em{flame retardant}.  The shirts usually go for 80 dollars, which is too much for me, but now I know that only one of my shirts will protect me from fire and I feel so vulnerable when not wearing it.

I don't have like a big fear of fire, I just like being prepared and once you know a shirt ◊em{can} be flame retardant, it's hard to not look down on all the shirts that choose not to be.

I am married to ◊wife{Angelica Blevins}.

We met through zines and comics.  She worked at a comic shop that stocked my zines, and would read them on breaks.  I read her comics and loved her art on all the different posters around town. That was a good 10 yearss ago that our friendship began, then our lives had a bunch of twists and turns, and we met again ◊anniversary{four years ago}.

I am a codewitch, barber, writer, tarotologist, and friend currently residing in Wellington, New Zealand. 

This is all I can think of for right now!


