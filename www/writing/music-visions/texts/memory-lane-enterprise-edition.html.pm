#lang pollen

◊h1{Memory Lane: Enterprise Edition}


The first Hella album I owned was a burned CD bought at their Capitol theatre backstage show.  They burned it themselves, wrote HELLA in sharpie on the disc and then stapled it inside a triangle of light blue paper and scrawled HELLA on that too. 

I do not remember the manufacturer of the CD-R, or the company that made the paper.

The first Tanya Tucker album I owned was an old vinyl 'Best-Of' that I bought at Rainy Day Records' sidewalk sale.  The cover art was faded, and had come off in a few sections showing the bright white paper pulp beneath, and there was a thick pad of price tags in the right corner, a history of markdowns and then markups.  The vinyl was thin and slightly warped, and made ◊em{Delta Dawn} sound pleasant and ancient.

I don't remember who pressed the vinyl though, or who manufactured the record sleeve.


I bought my first Jawbreaker Reunion tape at a zine fest at Silent Barn.  I went to the fest because I saw the Miscreant Zine had a table, and I wanted to say how much I liked their writing.  I didn't realize Miscreant was also a label, and so along with grabbing their latest issue I bought a couple tapes Jeannette, the zinester/label owner, recommended.

The tape had a download code inside, typed on a piece of paper that'd been obviously cut from a page of codes. The album was amazing, a new favorite band, and one of my favorite memories of New York was becoming dear friends with Jeanette, and performing comedy on a show with Jawbreaker Reunion.

And I never knew who manufactured the tape or made the paper the code was on.  It felt weird to ask?

The first time I heard the Twilight Sad was on a Pandora station, though I didn't know that was the band at the time.   I remember thinking that "man, I liked that song that just played", but I couldn't remember if it was the one that ◊em{just} played, or a few songs previous, and I couldn't go backwards due to what Pandora called  licensing restrictions. I just remembered it was on my "melodic but atonal, melancholic sentiment with vulnerable lyrics" station, so I hoped if I listened to pandora enough the song might come on again.

Later, I found a Twilight Sad album on lala.com and when the song came on it all clicked and I knew this was a band I liked.  I liked them so much I starred them and "bought" their album.  "Buying" on lala cost like $1.10 and you didn't really own the music, but could listen to it offline and without ad interruptions.

Sadly, lala was bought by Apple,  who shut it down immediately.

I got some apple credit because I spent money with lala, but I couldn't keep any of the albums I bought through them and the credit wouldn't cover any of them.

I could listen to Twilight Sad on spotify now, but they are always added to this "UK indie" playlist algorithm, which means every one of their albums ends by autoplaying right into an Ed Sheeran track and it's tiring.

I've only streamed their music, never had a more direct copy.  And because of this I know exactly which platforms I've listened to them on, and the platform's business and licensing history.  I have a keen interest in corporate mergers, since they directly affect my music collection.

I can't remember the name of Twilight Sad album, or even what it looked like.
