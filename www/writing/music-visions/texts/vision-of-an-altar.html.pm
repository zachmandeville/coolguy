#lang pollen

◊h1{Vision Of An Altar}

You've perfected a ritual for getting shit done.

An altar, a soundtrack, a few breaths, and your mind focuses to a crystal state where you can accomplish anything. You're so grateful for this ritual, tonight, with a midday deadline tomorrow and a blank page on your computer screen, its white pixel glow radiating like a stress headache.

You take a breath, you rest your hands, and you assemble your altar.

◊strong{Your  Altar}

- A clean desk
- A pot of tea with your favorite mug.
- A lit honeycomb candle near your computer
- The lighter in front of you, for a stress fiddle.
- A shard of selenite laid across the laptop.
- Your datradio opened in a new tab.

You've customized your datradio to radiate a cool calm.  A soft brown background and baby blue font in a curvy sans-serif you copied from Kyra's homepage, with small gifs of flowers laid in the four corners of the beaker window.  You've taken to writing your favorite lyrics into the datradio description.  Right now, in a thin gray cursive at the top of the page it says ◊em{'I'm not the moon, I'm not even a star, But awake at night I'll be singing to the birds'}.

And in the center of the screen is your dat collection, laid out in your perfect grid.

You'd found a tutorial in the chorus for how to customize your datradio collection so that each cover had the size and border of a postage stamp.  You loved that look and so you copied the source code and added it to your own radio.  Now you scroll through the stamps looking for your soundtrack, A Briana Marela album you'd discovered through a dat-zine shared on #song-islands.

When you click on the Briana Marela cover, your personal view fades away and is replaced by Briana's dat cover, and the images she'd loaded in.  Her dat cover is a looping gif of pink smoke rising from a field of grass, soft and strange and taking up the whole screen with the tracklist laid out at the bottom.

You watch the smoke rise, and take a few breaths.  This moment reminds you of when y'all used to hang out at Josh's house, and get high in front of his turntable.  A semi-circle of crossed legs, hands stretched out and sinking into the lilac carpet, the album cover propped artfully against the turntable like a display.

You watch the smoke rise over the field, you inhale.  You listen to Briana's voice echoing from one headphone to the other,  you exhale.

Then you go back to your blank screen and start typing.
