#lang pollen

◊h1{Greetings}

  This is a zine about music, communities, decentralized technologies, and building a future that makes your heart flutter.  It is about how coders can use their excitement about emerging technology to help their local music community flourish.  It is about how the best way for coders to build the future we want is to remember we are living, breathing beings filled with desire, just like everyone else in our community, and so the health of our local community should be our code's driving force.


This zine is a roundabout, decentralized tech, manifesto way of saying, "If your future has no music to dance to, I want no part of that future."

Practically speaking, this zine emerged when a friend (@cblgh) shared a cool decentralized music player they'd built called datradio.  I was excited by the possibility this player pointed to, and wanted to help.  My friend felt confident on the code side, but said they'd love help on the "long-term vision" side.

This is my attempt to help.

The zine is about coded things, specifically decentralized technology, but there is no direct discussion of code in any essay and no coding experience is required to enjoy this. I am focused on the ◊em{approach} we take with our technology, and the metaphors inherent in the tech we give each other--whether that's a cassette tape dubbed over with our favorite songs, or a codebase filled with all our optimistic functions.

This zine is not a guidebook, whitepaper, "10 point plan" or anything like that.

Too many tech folk love to decide the true fix to humanity's problems, a fix that always ends up looking the same and never seems to work well for the afflicted.  This zine is not a prescription.  It's my personal feelings and wishes for the future, but all my feelings just also have to do a lil bit with code.

If you've read to this paragraph and still wondering whether this zine is for you, let me tell you: this zine is for you!  I made it for you!  It's yours to enjoy!

The intro is over, let's get right to the first track!
