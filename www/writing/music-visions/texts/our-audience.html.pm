#lang pollen

◊h1{Our Audience}


Everything we create is for an audience.  If the thing we create is never shared, then the audience is ourself.  But as soon as we share it, we realize there was always an audience in mind: who is the first person we want to share this work with?  Who do we daydream stumbling upon this, and what we do we daydream their response to be?


Before we build a new decentralized music player, I think it's good to identify who we are building it for-- who is the audience that actually needs something new?  Why would they need it?

When we identify our audience, we also identify what makes datradio special.  We discover the problem datradio can help solve, and our motivating purpose.

◊h2{Is our audience the listeners?}

We could say datradio is for 'the listeners': the people who like p2p stuff ◊em{and} music and want to have a cool p2p thing for listening to their tunes...


But listeners don't actually need anything.  It's a wonderful time to be a listener, especially a casual listener!  Almost every album, going back to the first recorded sound, can be found online--and most of the time for free! Plus, all our old technology is still working--tapes, cd's, vinyl, mp3's, flac if you fancy.  There's nearly a century of music for us to listen to in a variety of ways-- it's truly a golden age for listening.

So let's  not make something for just the listener, they have enough.  The only problem for them is if new music stops being made, and they're bored with all the current songs.  But that's not a problem that can be solved by casually listening harder, or casually listening in a new decentralized way.

◊h2{Is our audience the Market?}

We could say we are making datradio for "the market".  That there's an abstract need for music apps that haven't been capitalized on yet, and we can be the first to corner this space.  The problem then is that there's an imaginary amount of money and prestige that we ◊em{could} have, but don't have yet; and we think that money could be found in our 'killer music app'.

But if that's truly your belief, fuuuuuuuuuuuck that, and stop reading this zine.  I have nothing to give you, and no interest to help you.  My best recommendation is to just look at yourself in the mirror, for as long as you can, and figure out why it's so hard to make eye contact.

◊h2{Is our audience The artists?}

YES.

Though, we should be more specific with this term.  Major label artists are fine, and longstanding artists are fine.  Like, I don't care about the Eagles and their music-sharing needs and it means nothing to me if the Chainsmokers never know our work.  They have as many options and resources as their casually listening fans. 

The people who need something different are the smaller, independent artists and the artistic communities they work within.  These are the people served least by our current music platforms, and their communities are hurt the most by these same platforms.


◊h2{Our Audience}
So then, who should be the audience for our decentralized music player?


It is the artist mustering up the courage to share their first song.

It is the band putting all their heart into their next album, the one they'll use to pay for their first west coast tour.  

It's the people running the diy venues in which these bands plays, the volunteers working the door, the friends running the merch table. 

It's the biggest fan starting a record label to put out their favorite bands' music.

It's the zinesters and mp3 bloggers documenting their scenes with reviews, diaries, and interviews.

It's the well-organized scenster booking shows, and the samaritans housing travelling bands.

It's the elders teaching youth how to play their first instruments and start their first bands.   

it's the store owners selling equipment and the librarians renting equipment. 

It's the dj's running radio shows and curating online mixes.

It's the graphic artists making album art and flyers and band logos. 

It's the coders who are a part of this scene, looking for their own unique way to contribute.

Let's make something for all of ◊em{them}, all of ◊em{us}.
