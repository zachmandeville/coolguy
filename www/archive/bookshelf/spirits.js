/* Welcome to the Spirit Page!  Here's all the javascript that makes
   our shelves fancy!
 */

var noteHeaders = document.querySelectorAll('.level4 h4')

document.addEventListener('DOMContentLoaded', () => {
  addCloseButtons()
  hideNotes()
  listenForHeaderClicks()
  listenForCloseButtonClicks()
  leaveNiceNote()
})

function addCloseButtons () {
  var notesSections = document.querySelectorAll('.level4')
  notesSections.forEach(section => {
    section.insertAdjacentHTML('beforeend', '<button class="hidden close-button">Go Back to Bookshelf</button>')
  })
}

function hideNotes () {
  var notes = document.querySelectorAll('.level4 p')
  notes.forEach(note => note.classList.toggle('hidden'))
}

function listenForHeaderClicks () {
  noteHeaders.forEach(note => {
    note.addEventListener('click', toggleNotesPage)
  })
}

function listenForCloseButtonClicks () {
  var closeButtons = document.querySelectorAll('.close-button')
  closeButtons.forEach(closeButton => {
    closeButton.addEventListener('click', toggleNotesPage)
  })
}

function toggleNotesPage (e) {
  var notesSection = e.target.parentElement
  var bookSection = notesSection.parentElement
  var notes = notesSection.querySelectorAll('p')
  var closeButton = notesSection.querySelector('.close-button')

  toggle(bookSection, 'fun')
  toggleAll(notes, 'hidden')
  toggle(closeButton, 'hidden')
}

function toggle (element, className) {
  element.classList.toggle(className)
}

function toggleAll (elements, className) {
  elements.forEach(element => element.classList.toggle(className))
}

function leaveNiceNote () {
  console.log(`
Hello!

Thank you for checking out the console!

This is the seeeeeeeeecret shelf.

These are my favorite books of all time:

    My Antonia, by Willa Cather
    At Swim-Two-Birds, by Flann O'brien
    Dog of the South, by Charles Portis
    Hardboiled Wonderland and the End of the World, by Haruki Murakami
    Bluets, by Maggie Nelson
    Complete Short Stories, by Vladimir Nabokov

Thank you for reading!
   ,   ,
  /////|
 ///// |
|~~~|  |
|===|  |
|j  |  |
| g |  |
|  s| /
|===|/
'---'
`
)
}
