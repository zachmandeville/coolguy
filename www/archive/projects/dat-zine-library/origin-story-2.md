I have been fed, inspired, and formed by independent DIY culture.  A large portion of my beliefs can be traced back to some zine, or song lyric, or comic panel, or house show (the remainder of my beliefs come from Judy Blume novels and various epiphanic moments beneath trees, rainstorms, stars, etc).  

Along with every other gross thing caused by the current style of the web, one of the most personally terrible for me is the atrophy of independent culture.  People are still being wonderful and creative and alive today, but they are also so _tired_.  Artists quit at the height of creativity because they are worn out, and it's not being worn out from touring and excess or some such.  It's the wearing out that happens when you have to constantly promote yourself to algorithms that don't care about you.  There's so much energy being drained by 'the internet', but the support structures that were present in independent culture are being replaced by weak digital fascimiles, and it's becoming harder for artists to help each other or find a way out.

This feeling that the web is working against us is larger than just diy circles.  Because of this, there's a lot of work being done by really cool people to offer an alternative to the web.  This alternative is decentralized, not reliant on any central server like google to operate, and it is being designed to undermine and dissolve oppressive systems like facebook (and the state). The future I want is decentralized, consensual, anarchist, feminist, and cool--and luckily this is being built right now, a new space different from the web that I lovingly call the Chorus.

I wanted to help contribute to the Chorus, using the little bit of code I know and the lotta bit of heart I have.  And I wanted to do so using the style I grew up in, and so I am trying to foster punk and zine culture in this new future space. You can check out the first library (and create your own!) here:
dat://dat-zine-library.hashbase.io
(This requires the beaker browser to view, but you can download it [here](https://beakerbrowser.com)

----
That's the basic origin story.  If you wanna know about my intentions and feeeeeelings about tech today, then read on baybeee!

# My Intentions 
 
I wanted to avoid a tendency I see so much in new technology or platforms, which could be called 'the DuPont, WA problem'.  To explain this, I gotta tell you a bit about the town of Dupont, Washington and my feelings for it.  And so...

## A QUICK SHITTING UPON THE TOWN OF DUPONT WASHINGTON

I grew up in the town of Funwater, Washington- a rural, magical, college town about an hour south of Seattle.  Dupont was a newer city about 20 minutes north of me.  It was a small town that had been rebuilt by Intel as a place to house its employees, and then modified by the local military base to hold its soldiers and their families. The town was almost entirely artificial, a perfectly designed vision of an ideal American town.  This design made it so DuPont was boring as heeeeeeellllll.

I spent a decent amount of time there, I don't wanna be some snob shitting upon towns they merely pass on the freeway.  My brother and his family lived there, and my parents loved eating at the 'Irish' pub in the 'downtown'.  There was a lot of desire in the town to be nice, but it was still just soulless and boring.  Everywhere was easy to get to, the sidewalks were all wide and walkable, the street signs clearly marked, each intersection a roundabout and ample parking everywhere.  It had everything necessary to be a city, except for culture and heart.

A downtown was built for Dupont, with a bunch of empty spaces advertising how great a location the city would be.  Soon after, a bunch of chain and franchise businesses opened in these spaces, since there was no real local culture to rise up there instead.  So the downtown had an illusion of life, but it was really an anonymous stripmall. 

## WHERE ARE YOU GOING WITH THIS?

There's this tendency with a lot of technologists who want an alternative to the gross platforms of twitter and facebook, so they build a _new_ platform, with a more noble mission statement, and hope the cool people arrive.  Because it's a platform  for people to interact in, the new thing isn't really _a thing_ until enough people are on it, so then the developers spend all their time trying to make their new site super easy to get to, super easy to get signed up for, with little addictive tricks to try to keep people engaged.  In other words, they're building all these onboarding ramps and roundabouts and lit up street signs to show that they are a real town that you can build  home in. 

The problem with this is it still puts so much labor on the users to make this thing cool, without giving the users any actual power.  Someone wants something better than twitter, so they try out this new site called peach.cool or imzy.com or ello.co or steemit.io or whatever. And the splash page for the site is wonderful, and the mobile app is slick with super happy messages, but  then they get inside and it's just an empty place with maybe a few users trying to self-promote their new blog. If they  want their new online home to be interesting, they have to start petitioning all their friends to come over.  Since there is nothing in this new space yet, it's hard to make it sound appealing.  It's like someone trying to get their friends to move with them to DuPont, with the appealing "All the houses are super close to a freeway entrance, and there's a grocery store with front and _back_ parking.  Oh and you _know_ we have a Starbucks, within driving distance!" 

Ultimately, the new platform shuts down, since it was ultimately a business with a business model reliant on have as many people as possible.  The tiny early crew was not enough, and they now have to find some other home or go back to their old ways.

This is a 'Bad Model'.  It doesn't make people's days easier, it doesn't make people less tired and more happy--and it just creates a string of digital ghost towns, web pages built like Dupont Washington.  These platforms are exhausting for artists too, as they now need to promote their own work _and_ promote this new web platform, but there's nothing really to distinguish this platform because it was designed to be familiar but 'better intentioned'.

If we want something that is actually different, then let's not make comfortable, familiar shit built for addictive convenience that is boring and condescending and ultimately identical to what came before.  Let's stop making high-tech versions of Applebee's.

I don't want to code an Applebee's, and I don't wanna help create a digital DuPont. I much prefer the isolated smalltown punk scene method.  Don't build for the masses with the goal to be the next dominant force.  Instead, make cool things for your friends to enjoy, and help them build cool things too. Work with what you have, to support the people around you and together you'll create a community that has a defined shape and form only in hindsight.  Instead of worrying about having enough onboarding ramps, I say we make a future space that is so exciting, so fun, that is such a cool party with lights so bright that everyone wants to build their _own_ methods to get here and join in.

And I thought: what's the coolest, most party thing in the world?

Reading.

# Dat Zine Library

Dat and Beaker are two major elements of the Chorus.  Dat is a protocol (similar to http on the web) that lets you create and host a webpage straight from your computer.  [Beaker](https://beakerbrowser.com) is a browser designed for dat, it helps you make your own dat sites along with viewing and supporting the works of your friends.

Coding your own web page, even with the simplest 1997 geocities html, is one of the most powerful and liberating feelings.  I cannot recommend it enough.  But hosting the web page so others can see it can be mad time-intensive or money-intensive.  This is a bigger hurdle than learning the code.  Beaker and dat eliminate this hurdle.  In the Chorus, your computer and your friends' computers are the host, and writing and sharing a thing can be folded into a single act.

This means the only entrance fee to having a site up is having access to a computer and internet connection. There is no hosting heirarchy or contortion you have to go through to fit some other publishing platform. And with this, it means you don't have to make just a single webpage.  It's easy to make a _bunch_ of dat sites, each centered around whatever subject you'd like.

There is no implicit way to discover dat sites--instead you have to share your link with friends and hope they support, seed, and share the link too. A dat site spreads, then, through classic, social and 'underground' channels. 

These things combined  made a datsite feel far closer to a zine than a webpage.  There is no hierarchy to who can write them or publish them.  There is no strict definition for what they should look like or what they  should be about.  And to visit a dat site, much like reading a zine, requires that you ask for it from the creator of it (or be a part of a culture that is supporting and sharing them).

Also, dat sites have an ability to be copied by any of the visitors to the site. If you make them the right way, then your dat site can be both a thing to be enjoyed and an instruction manual on how to make one yourself. This dual-purpose is the heart of what I love so much about zines and punk culture.  It is hard to be passive in a punk scene, everything is inviting you to join in in whatever way you can.

## Making Dat Zines

This got me really excited, and so I wanted to start making small dat sites that emphasized this independent spirit. Each site has a different codebase, different subject, and different look--but they are connected by a shared standard in their file structure. Each page has a distro folder which holds instructions for distribution, which holds the author's intentions however they want to state them.

I wanted to have a way to gather the zines I made or found and wanted others to share: what would traditionally be a distro or a zine library. Since I was not selling these, and wasn't also selling like tapes and pins and such, a distro wasn't the right fit. So I coded up a dat Zine library. 

## Dat Library Spirit

To be clear: this is not a platform. This is a tool and a suggestion of spirit.  The first dat zine library is here: dat://dat-zine-library.hashbase.io.  It is my personal library, but you can copy it with a button click and have your own personal library too.  My library is filled with the zines I created, ones that my friends made, or ones that were shared with me that I liked.  Yours can be filled with whatever you want, in as wide of subject matter or as specific as you want.  The core intention of the zine library is to excite you, the visitor, enough that you want to make a zine yourself and then to find enough examples for how to do so. 

My dream, is to have people inspired to make webpages again about whatever they'd like, and share them in ways that don't promote competitive, addictive 'engagement stats'. And to have cyber-regional zine libraries that are collecting and supporting different scenes'  work. That may come in the future.  For now, I have a small set of nice things that I made with friends, and a place where I can make more cool things with these friends.  

The Chorus offers a new way to support a thriving independent culture again, one made by and for artists and their communities--- These dat-zines are my personal expression of how that could look.

# That was some grand talk! Bring it Down with some Honesty!

I have a hard time containing enthusiasm.  This can be annoying, because it means when I enjoy a thing it's hard to suppress the urge to recommend this thing to everyone else too, saying this thing 'is the most important change the world has ever seen'.  I did it with the movie Pacific Rim, I did it with boardgames, I did it with the dish 'vegetable curry with roti'. I'm probably doing it now with this page.

But to be honest, I made the library, and these initial zines, as a personal act to get out of a depression.  Writing zines was a huge part of my life and identity, but I lost the spirit for it when I tried to move the work online.  The act of promoting, and legitimacy, was mad distracting.  I was concerned far more with page stats of my blog, or the performance of a fb post or tweet, then any new work.  Which was silly, because I started a blog and twitter account because I thought I needed both to promote my work. I am not saying this is the same for everyone, but just how my head works.  When a site is designed to notify me with how well I'm doing, or how popular I am, I become discouraged by the concept of both and immediately want to disappear taking all my work with me.  It's worse when the site is holding all my creativity, and needs my engagement to survive and continue to preserve my work, and so I have to actively engage with the worst parts of my head so the best parts can persist. 

I love making things, and sharing things, and knowing that something I made resonated with someone else. I wanted to figure out a way to do this that didn't throw me down a bad spiral. Coding up dat sites brought back an energy I missed for a v. long time, and I wanted to share that with you.
