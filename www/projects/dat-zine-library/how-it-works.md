# How it Works

The Dat Zine Library is a lot like the comic strip Heathcliff, in that it works on many levels.  You can enjoy the library as a patron, as a zinester making a submission, as a librarian tending to a branch, or as a librarian hacker spinning up their own version.  I'll try to lightly guide through each scenario!

# A Word at the Start
 this is a tool of the *_~~*future~~*_, and no matter how you interact with it you'll want to know some future tools and terms, as I'll likely be using them throughout here.
 
## FUTURE TERMS

- **The Chorus**:  This is the place in which the zine library resides, instead of 'the web'. The Chorus is a space of radical, punk2punk art and software that aims to help build our solarpunk future.  It's decentralized, so everything can be done through the community without relying on any central server or authority. Every member of the zine community is an integral part of the digital infrastructure, and your support of a zine in the Chorus directly helps the entire infrastructure.
- **A Zine**: A mix between a love letter and a book.  Zines have a loooose definition, cos their power cannot be contained, but generally they are self-published pamphlets that are distributed through a network of friends, cool librarians, and punks.  They can be about whatever you want, and look however you want. 
- **A Dat Zine**: A mix between a zine and a website.  These are self-published dat sites that are distributed through a network of friends, cool dat zine librarians, and solarpunks across the Chorus.  They can be whatever you want, coded however you feel comfortable coding, and look however you want.  They don't need to be text-based.  A cool coded rhythm game could be a dat zine if the spirit is right.
- **Dat**: A protocol for sharing sites, files, and apps in a distributed, decentralized way. It is similar to http, which is the protocol the web uses for sharing web pages.  But in the web, pages are held on servers that are then accessed by the site visitors.  With Dat, the pages are held on the devices in which they were written, along with the device of every visitor and peer who chooses to seed that site.  In this way, there is no central server controlling access, the content lives among the community itself.  It is kind of like how torrents work, and kind of like how the web could've worked, but different than both.  Because it's a separate protocol from the web, most browsers (like chrome or safari) cannot view a dat zine.  There is a new browser, called Beaker Browser, that can view dat sites _and_ create them.
- **swingin' on the flippity flop**: A slang term for 'hanging out'.
- **Beaker Browser:** A web browser for the Chorus.  It can be downloaded at [beakerbrowser.com](https://beakerbrowser.com).  It can be used to browser the normal web, but can also view dat sites and dat zines.  In addition, it has editing and publishing capabilities built in, so you can create your own dat zine from within the browser.  
- **Seeding**: A technical method of support.  Since the zinester who made the dat zine also acts as the 'server' hosting the zine, the zine may not be accessible if their computer isn't on and Beaker isn't running. You can support their work by clicking the seed option on the beaker browser, which turns your browser into a server for this zine too.  Now people can access the zine from either of you.  As a zine grows in readership, it also grows in reliance and availability and you can show support to the author in a directly felt and appreciated way, that isn't just monetary.
- **Forking**: An integral, revolutionary part of Dat, Beaker, and the Chorus. Forking means to make a copy of all the files that make up a dat zine, but this copy you have 'write access' to. In other words, make your own copy that you can edit in any way you please.  All dat zines can be forked, which means you can be inspired by someone's work, and make a new zine based off their structure.  In fact, many of the existing dat zines act as both a zine and an instruction manual for making zines.  In the same way, the library can be forked so you have your own branch and you can change up the collection however you please.
- **HTML/CSS/Javascript**: The paper, sharpie, gluestick and scissors for Dat Zines.  There is no expertise level required to make a zine.  You can do it with the most basic html that you've gathered from old geocities pages.  If you want to make a dat zine, but have not coded at all before and feel intimidated, feel free to [email me](mailto:webmaster@coolguy.website) and I'd be happy to help where I can!
- **Scuttlebutt**: sea-slang for 'gossip', but also the solarpunk community/protocol of our beautiful future.  Scuttlebutt is an incredible way to discover dat zines and talk to other zinesters about it. You can learn more at [scuttlebutt.nz](https://scuttlebutt.nz)
- **Text Editor**: Literally a program to edit text, but usually refers to one that's meant for writing code of any sort.  You will use a text editor to write up your zines.  The beta version of beaker has one built in.  There are also free text editors available that are mad useful.  [Atom](https://atom.io/) is a good one, as is [Vim](https://www.vim.org/).  (_note: Vim has a bit of a learning curve, but it's a beautiful one and can be a life-changing experience. I fuck with Vim._)
- *Command Line or Terminal**: The sacred well hidden beneath the ornate cathedral of your computer. The terminal is available on all Mac and Linux computers, and can be emulated on windows.  It's an entirely different way of interacting with and talking to your computer, coming from a more radical and beautiful past.  Using the command line can be strange at first, but is incredibly empowering and lets you discover new tools and methods that are hidden away on your device.  You likely won't have to use it as just a zine fan, but it's mass recommended when you become a hacker librarian.

Alright, them's the terms lets get to some teachings!

# Visiting the Library and Reading a zine!

To get to the library, you will need to download and install beaker browser.  You can find instructions for that here: https://beakerbrowser.com/install/

Once you have it up and running visit this site:
**dat://dat-zine-library.hashbase.io**

Here you'll find all the zines I've written or collected.  When you click on the cover for one, you'll be brought to that zine in a new URL.  

If you click on it, and nothing shows up, that's likely because I forgot to seed the site, and the zinester is not online at the moment and thus the zine is not accessible.  Check back again in a bit, and it'd likely be up!

## Good Etiquette

If you like a zine, click the three connected dots in beaker's address bar and choose a seeding option:
![gif showing how to seed a dat zine](images/seeding-a-dat-zine.gif)

This supports the creator by helping make it available for everyone else, reducing their hosting burden.  Similarly, if you like my zine library, please seed it too!

# Making your own Zine!

There's a few ways to go about this.  You can either start from an existing dat zine, especially one intended to work as a template, or you can make it entirely from scratch.

There's no right way to code a zine, and there's no specific format for it.  There is a unifying standard _folder_ though, that marks that specific dat site as a _ziiine_.  It is the **distro** folder.

The distro folder has two files:
- cover.jpg (or cover.png or cover.gif)
- info.txt

The image you name as the cover will be used in zine libraries as your cover.  The info.txt file contains any information you want people to know about yr zine, and it's displayed at hte bottom of yr zine card in the library.  It's a good spot to put the author, the creation date, sharing requests, the music you listened to during the creation of the zine, and so on.

While this info.txt file can contain whatever type of info you want, it is written in a v. specific style that looks like this.

```
title: My cool Zine
----
author: Doris Cometbus
----
pertinent things I wanna say:  Whatever pertinant thing I wanna say.
----
anything else: 
yep anythign else!  
----
```
In other words, the subect, followed by a colon and whatever it is you wnana write, and then you divide each subject with `----`

If you need more guidance ont this, I recommend checking out the distro folder of current zines for inspiration.

## Making a zine from a template

There's some _dat zine styles_ already out there, which are zines whose structure functions as a template and teaching tool for making your own.  Here are two I recommend:

- **Satan, a Real Georgia Peach**: dat://f9c9b6ae86180c119d23b452eefaeb8a494a6c4222e5588f908f5f5ba96f6195/
This is a comic, that has a cover and back and a long scrolling comic in between.  If you click the info button in the top right, you'll see a fork option appear.  clicking that will make your own editable copy.  Then click the three linear dots in the address bar and choose 'view files'.  The readme will guide you from there.   In short, you can make it your own without knowing any code, by just changing the comic pages with your own and updating the distro file.  Or, you can change the styling and everything else (the readme goes into more details.)

- **read me**: dat://d0222b1bcf7833e0708aa65ba8376aadba3f13e3025a113b10f755342b4c14d1/
This is a short sci-fi story, and an example of a text zine that can have multiple pages.  Just like the comic, click the three dots, make an editable copy, and then check out the readme for editing tips!

## Making a zine from scratch

Have at it however you want.  You can do this straight from Beaker by clicking the hamburger icon in the top right, then choosing 'create new > website'.  This will give you a starting template to go from.  The next version of Beaker has a text editor built in and you can create your zine from directly inside the browser.  If that's not available for you,t hen you'll want to save these files to your computer and edit them with your text editor of choice ([atom](https://atom.io) is a good one).  

To save it to your computer, in the bottom of the file view in dat you'll see a button for 'set local directory'.  Click that, choose where you wanna save it and then open it from there.

At this point, you already have a dat site.  It's the long cipher string in the top right of your library view.  As you make changes in your local files, your site gets updated automatically.  When you're ready to share it, make a distro folder and then please send the url to me!  I want to see it!!

# Becoming a Zine Librarian

On my library, beneath my description, you'll see a button to make your own branch.  Just click this, and it'll bring up the Forking option.  Name your new branch whatever you'd like, and when you press OK you'll be brought to your own library.

The view should now change for you, where you'll see edit buttons everywhere.  You can now edit the branch details, add new zines, and edit the collections for existing zines.  You can also view the files in the backend.  The primary one to check out is the zines folder.  This contains all the zines you've added in nice text files.  In beaker, you can right-click each one to delete it if you want, or select multiple and delete them all if you want a totally fresh branch.

Then, check out the folder `building/aesthetics`.  This contains the CSS stylesheets for the library, and is the simplest way to customize things to make them look like yr own.  Change the library colors and fonts around, or change the css entirely.

The big thing here is to send me that new branch when you ready!  I want librarian friends and a full on library web ring, and I'm excited for the zines you make and find.

# Becoming a Librarian Hacker

When you've forked your own version, you can edit any of the code you want.  A large amount of this is explained in the readme.  Here's some basic tips.

To change the overall css, check out building/aesthetics.  To edit how the library functions, though, you'll need to to do some javascript.

The library is written using node and choo.  So you'll want to make sure you have node installed on your computer, and then after you've made changes to run the command `npm run build`.

I'm kinda running outta steam on writing this right now.  BUT!  If you want to hack on the code itself, but don't know what node means or where you'd run the command `npm run build`, then please email me and I can help out further! (it also will let me know that someone read this far and needed the info I hadn't yet provided.  Until then I'ma sleep!)



