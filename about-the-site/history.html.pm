#lang pollen

◊define-meta[title]{About Coolguy.Website}
◊define-meta[subtitle]{History}
◊define-meta[background]{peachpuff}
◊define-meta[color]{indigo}

◊sect["Ch. 1" "A Branding Page"]{
Coolguy.website started out as a promotional site for me and my comedy career, made with the web-building software ◊link[#:href "https://webflow.com" #:alt "external webflow homepage"]{Webflow}. I felt I needed a professional site and had tried to make one in squarespace, but was immediately frustrated by their aesthetic and design restrictions.  I liked that Webflow let you build up a site from scratch with seeemingly complete control, but you could do so with an intuitive visual editor.

I knew nothing of html at the time, and was grateful for Webflow's ui.  They did a great job at making the code transparent (calling a div a div), while still being approachable.  This set off a spark in me around web design, and how it could be a personal creative practice.  I have fond memories of waking up excited and immediately turning on my computer to work on coolguy like I was a kid with a new video game.

Webflow's target audience is web-design companies, and their pricing and priorities reflect this.  After my intro period ran out, it became too expensive to keep using.  I also did not like how my site was bound to their service, and could only work on it within their UI, on their preferred browser, when the internet was up.

There's an archive of the site from this time on ◊link[#:href "https://web.archive.org/web/20161106213334/http://coolguy.website/" #:alt "external copy of coolguy on archive.org"]{archive.org}.  It's easy to see the influence of website builders, in that the site functions mostly as a creative business card and portfolio.  I found that I did not enjoy updating its content, but loved updating the code, which started to give me some subtle existential crises (as subtle as those can be).

◊image["webflow2.png" "screenshot from original site"]{Green site with cool stuff and all of that.}

◊image["webflow1.png" "screenshot from original site"]{Green site with cool stuff and all of that.}
}

◊sect["Interlude" "Pinball Brutalism"]{
While I was feeling my creative energies shift, I had a synchronistic, magical, life-impacting weekend at the Pinball Hall of Fame in Las Vegas.  Sincerely, coolguy today would not exist were it not for that beautiful arcade in that shitty city.  I'll keep it brief, but essentially I had a random moment where my past and present homes and selves all overlapped, I randomly ran into two friends, Brendan and Luke, while stuck in the city and so we decided to visit the pinball museum. While there, Luke brought up the topic of “web brutalism”, which I had not yet encountered, and Brendan waxed nostalgic for being a teen making sites with just notepad which…I kinda forgot you could do?  And while this conversation was happening, I was playing a beautiful tic-tac-toe pinball game from the 60’s, all aglow from its modpink light, while next to us an elderly hippie worked on a broken machine, its top lifted to show the chunky, but elegant innaards.  The caretaker wore telescoping jeweller's glasses and a forehad lamp and delicately tinkered with the wires.  I had one of those overwhelming moments, like when the sky is all blue and seems to reflect you, and I thought, “I should make a website by hand.”

On the flight home, I read through a  number of articles I'd downloaded about web brutalism and handmade webpages, and it was in one of these that I discovered ◊link[#:href "https://tilde.town" #:alt "external tilde.town homepage"]{tilde.town}.  I sent a user request as soon as I got home.

When I got accepted into tilde.town, logged into their server and updated the html on my site directly, it was like a flower bloomed in my head.  Then, a few days later, when my friend Kalen showed me how to set up git so I could code on my own computer, and then introduced me to the text editor Vim, that flower became a garden.

My tilde.town page is still up at ◊link[#:href "https://tilde.town/~zach" #:alt "external link to zach’s tilde town page"]{/~zach}.  It is basically coolguy.website made with the html version of crayons. I have not logged onto tilde.town for a while, but I will forever love that community and their magic server.}

◊image["tilde1.png" "screenshot from original site"]{Green site with cool stuff and all of that.}

◊image["tilde2.png" "screenshot from original site"]{Green site with cool stuff and all of that.}

◊sect["Ch. 2" "We Know And Love"]{
I reached a junction where all my fun web energy was going into tilde.town while coolguy had become a stagnant (and expensive) webflow account. Coolguy wasn't cool, it was a business card and branding that no longer reflected me and showed. So I decided to restart coolguy from scratch.  I wanted to keep a lot of my writing, but would code the site from a blank vim screen.

Philosophically, I was inspired by the books ◊book[#:author "Tung-Hu Hui"]{A Pre-history of the Cloud}, ◊book[#:author "Jaron Lanier"]{Who Owns the Future?}, and ◊book[#:author "Margot Adler"]{Drawing Down the Moon}.  Prehistory especially changed me, I carried it around like I was 10 and it was a book about ghosts

(sidenote for this simile: when I was 10 I carried around Daniel Cohen's ◊book{Encyclopedia of Ghosts} everywhere I went. I realized this wasn't a common memory, descriptor.  I hope the enthusiasm, and importance, I felt about ◊book{A Prehistory of the Cloud} came through still.)

Before Hui's and Lanier's books, I believed that computers were like pre-destined, and inherent to the world--as if the makings of the internet always existed and we just discovered it one day.  This wasn't a conspiracy theory of mine or something, all thinking there were computers in the background of hieroglyphics.  Rather, i approached technology as this thing you just had to understand, that its methods were immovable and you either got it or you didn't.

Prehistory and Who Owns the Future revealed a different history to me.  Technology is a layering of agreements and arguments.  There was not one way all this could have gone, there's multiple paths and the story of tech is one of deep philosophical tensions more than cold truths.  I could fuck with philosophical tension, I could understand computer technology as a shared human expression, as messy, emotional, and incomplete as all other forms of expression. 

◊book{Drawing Down the Moon} is a study of neo-pagan and magical traditions in the 20th century, and beyond just being an aesthetically powerful nostalgia for me (kind and nerdy witches using desktop publishing software to send out a newsletter about earth magic to mid-80's pagans), it showed how powerful it is to just start something.  If you have a want for the world, express it and start practicing as if it already exists.  You'll find the people who need this world too, and the community will be your wish reflected thrice-fold.

Beyond these books and tilde.town, the biggest inspiration for coolguy was the underwear packaging at Uniqlo, specifically their Airism line.  The ads and packages all had this pale-blue, calming box pattern that reminded me of blueprints, medicine, and 90's emo covers.  I bought a pair so I would have a reference label, then tried to emulate that look in coolguy. This is still the foundation of the site's aesthetic today.

◊image["webflow2.png" "screenshot from original site"]{Green site with cool stuff and all of that.}

◊image["webflow1.png" "screenshot from original site"]{Green site with cool stuff and all of that.}

Another profound influence on my site (and life) was a hacker brunch we started to attend in Queens.  It was run by a group of friends practicing radical tech autonomy.  They quickly became my dear friends and helped me build the skills necessary to maintain the site, but also to articulate my own feelings around computers.  They also recommended the work of ◊a[#:href "https://techgnosis.com/" #:alt "external, homepage of erik davis"]{Erik Davis}, and his book ◊book{Techgnosis}.  This book became a new talisman and the underground current for coolguy.

My friends are now running a space called ◊link[#:href "https://shiftctrl.space"]{I/O} and if you are in NY, you should definitely check it out.  If you are not, still check out the site becaue it is inspiring and filled with hidden depth.

I wanted a home online that felt like a phyiscal personal home, a place to burrow and recharge. It was a hermitage.  I wasnot expecting how many nice visitors I would get! I don't know my actual hit counter, but many people have emailed me the sweetest things, or mentioned my site in person, and each time it makes my whole body feel like one big beating heart.  I'm especially touched when people reference some snippet of HTML or CSS in the site.  This is something I hoped for, but never expected to happen.

There's a joy that comes from working on this site that I haven't found in many other places.  Knowing that it started as a single blank screen and moved outward to this colorful, moving, thing.  When I am on a page I can see all the styling as a visual object, but also remember the exact bit of messy code I wrote to achieve it.  It feels like a sort of synesthesia--- which feels full-on corny to write, but also honest!

This overlap of code and words also meant that I found I was working on coolguy a lot, but not really "updating" it.  I'd work on tightening up the CSS to be more articulate, or I'd change the way I uploaded files to my server, or would build a script to help update the calendar.  I started to do this more than providing new pages or words. I realized I was having these fantaastic experiences around coolguy.website, and it was stirring up all these emotions and dreams, but I wasn't articulating any of them.  Coolguy started to not feel like me, or felt like a frozen expression instead of a lively home.

I wanted to find a better balance in how I inhabit this space, which sparked up this desire to overhaul and start again. Which is coolguy chapter 3.
}

◊sect["Ch. 3" "Today"]{
I have this problem/charming quality/total aries aspect of myself where, when I find something I like, I try to get all my friends onto it.  I enthuuuuuuse too much. And It's always good-hearted--- I want all the lovely people to experience all the lovely things, but it's maybe annoying.

And so I apologize, BUT!  I think everyone should have a personal website.  They are just the best.  They support and encourage me and help me with sadness in a way I did not expect.

When I first started coolguy I proudly had no clue what I was doing.  So much of the site didn't work, and I celebrated it.  This is not who I am today. Working on all this made me better at coding and understanding how everything works.  I still love all the warts and mistakes on here, but it would be dishonest and ungrateful of me to still be going "Aw shucks, what's a CSS file?"

The code on my site articulates something I've repeatedly tried to say in words.  Both forms of expression point towards the same thing.  But as I grew more confident, and expressive, in the underlying code I found that I was censoring myself in which words I shared or how I shared them.

To be direct, I wanna talk about websites and code a whole bunch!  There's so much wild history and cool dreams within this place I'm exploring, and I got so many "big ol' opinions" about it.  I've only let a tiny amount out, relatively speaking.  I avoided adding these words t coolguy because:

1.) I'd had an outdated notion of what my online house should look like.
2.) I fucking hate techies and techie websites.

There's a type of insular language used in so many of the sites that feels like a  group of people sharing inside jokes from last weekend at the country club.  It's just the most boring weekend at the worst country club.

At same time, there's all these beautiful pockets online and in tech's history, people who endlessly inspire me.  There's documentation for websites that helped me achieve all my misty ideas.  These pockets are just as much part of the internet's cultural history as all the biscuitheads in Silicon Valley.

I don't want to self-censor, nor keep an outdated notion of what coolguy should be.  I also feel an intense gratitude for everything I've learned, and don't want to hoard it.  I hope there is someone I don't know who feels the same way I did about computers two years ago, and finds coolguy.  And maybe they'll stay here awhile and some aspect helps them manifest their own home.   I want to help this vision, and I know that avoiding tech subjects or interets is not ultimately helpful.

So I rearranged the furniture of coolguy, and put forward a different wish on its altar. Much of what I want this place to be isn't here yet, of course. The last chapter of coolguy started as a bunch of ideas and a single white webpage.  I'm optimistic for what this current chapter becomes.
}
