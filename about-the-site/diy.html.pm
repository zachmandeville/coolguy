#lang pollen

◊define-meta[title]{About Coolguy}
◊define-meta[subtitle]{DIY}
◊define-meta[background]{cornflowerblue}
◊define-meta[color]{#ffe8e8}
◊(define-meta mashnote ◊@{
                         something or other that i want to say in a source code
                         block that looks real cool at the top of the page is where i would put it
                         if that worked okay for you.})

◊sect["intro" "wishing for a webring"]{When I started coolguy, I did not have some personal ideal for my website. Instead, I had an image for the type of site I would like to visit. Which is to say, the desire beneath all of this was not to have a homepage, but to have a neighborhood in which my homepage belonged.

This is still my wish. The web today makes me feel lonely. All the major sites have the aesthetic depression of a stripmall, and when we communicate through them our words come out muffled and distorted. I made Coolguy so there’d be a place where my words felt clear and the aesthetic my own, but endlessly tinkering on your own homepage start to feel like agoraphobia. I want my friends to all have their own websites, so I have more homepages to visit.

When I started, I remember looking up guides online and finding everything so distant and confusing. The thing I was trying to do felt simple, but I couldn’t find an easy answer for “how do I get my webpage online”? The guides talked of pipelines, and git hooks, and frameworks for development which was prolly great advice for a business, but I was trying to accomplish something 13 year olds did 20 years ago. I was lucky to have coder friends who generously helped me get oriented.

And so now I want to share how I get coolguy.website online. This isn’t a step-by-step guide, more a series of pointers and key words to explore. If you want to start your own webpage, I hope this essay can help set a path for you.}

◊sect["primer" "website essentials"]{
                                     At its core, to make a webpage you need three things:
◊ul{
    ◊li{something you want to share}
    ◊li{a server to share it from}
    ◊li{an address to that server you can give friends.}}

Before going further, it’s mportant to remember that all of this (the web and the world) is just text and agreements. I mean this in a magical sense (we manifest words into worlds and we are the worlds our words manifest), but also in a practical sense. The internet is entirely made up, it’s technical components are just desires and concepts that we gave names to. For example, a server isn’t a specific kind of machine...it’s more a description of a need that can be manifested in all kinds of ways. This can be liberating, but also frustrating. It means that so many guides will have something like, “We call it this, but technically it’s that,” or, “This is what you want to do most of the time but there are exceptions, and to be technical, you’re not actually doing anything at all.”

So any confusion you feel is understandable as the language around the web is inherently confusing—- we are literally making it up as we go along. How I describe this is not authoratitive, just my own personal understanding of how this works. You’ll develop your own way of understanding and it will be equally accurate.

So to return to a webpage: the server is often called “hosting”, and the address you give your “domain name”. The core need around your webpage is getting the stuff you want to share hosted on your server, and then assigning your domain name to that server. There are a multitude of ways to accomplish this. I chose my method because it was transparent, empowering, and cheap.}

◊sect["prelims""cost"]{
                       
Running coolguy.website costs me $36 a year. This covers the hosting costs of the server, the licensing of a domain name, and all the tools I use for coding and moving files. It’s possible to bring this down to $0, but my current setup is a good balance between ease and capability, and so I don’t mind paying $3 a month.

My domain name (coolguy.website) costs me 12/year.
My hosting provider costs 24/year.
Everything else is free.

I am lucky that I got a computer for free (given as part of a job, and I was able to keep it after I left). You will need access to a computer to make a site, but you don’t necessarily need to own it. My whole flow can be done using a raspberry pi, which retails between $5 and $50. Schools and hobbyist friends often have a surplus of pi’s and happy to give them to a good cause, so it can be easy to acquire one for free.

All the software I use is decades old and in most cases pre-installed on my macbook. Once you know where to look, there’s a whole world hidden in your computer; methods, traditions and languages waiting to be called upon that that give you the power to author anything. They do not require licenses or cloud access or anything like that. Making a webpage is a form of personal expression,and anything that makes you pay to express yourself is morally suspect to me.}

◊sect["part one""the domain name"]{
This is the first thing I bought, the domain “coolguy.website”. I bought mine from the site ◊link[#:href "https://nearlyfreespeech.net"]{NearlyFreeSpeech}. I like them because their site feels regional and almost...rural to me? There’s a straightforward aesthetic that reminds me of electricity cooperatives and community centers. The main reason I chose them, though, is that when Angelica and I first started dating she told me this was the service she used, and everything she did was(is) cool to me. So I went with NearlyFreeSpeech as a form of flirting.

To use them, you’d sign up for a free membership, click the domain tab in your member’s page, then click “register new domain”. You’ll choose the name of your site and it’s suffix: e.g. .com, .website, or .rocks.

◊sidenote{An appeal of NFS for me is they are barebones and direct, only adding features if you ask for them, which means their base price is always low.}

A site’s suffix is called a TLD(top level domain if you curious), and each TLD will be a different price because capitalism. NearlyFreeSpeech doesn’t have full control over these prices, but their rates are all cheap compared to other sites I’ve seen. You’ll want to find some combination of name and suffix that is unique,multi-worded, and affordable. I find that the TLD’s .website, .party, .xyz, and .rocks are phonetically pleasing and all relatively cheap.

◊sidenote{I say multi-worded because there’s a bunch of single-name domains that have been algorithmically squatted on and had their price boosted for no reason. Something like dream.city will cost $5000, but dreamtime.city will be $11.}

With domains you are not buying the name, you are buying the privilege of deciding where that particular name should point to. You’ll need to renew this privilege each year like a license. This is the worst part of the whole web thing for me, just cos it feels really bureaucratic. At the same time, I love being able to say “find me online at coolguy dot website”, and that I can use this name for all sorts of things (email, main website, dat zines, etc.) Having a secret name that you control is super powerful, and so I recommend pushing through the teeth-gnashing part of it.}

◊sect["the server" "broadcast your site"]{
I use NearlyFreeSpeech for hosting too. On the same members page, you can click on the Sites tab and make a new site. Their default option is great, and nicely extensible.It will work for your handful of handmade pages, but can grow if your needs grow. A site costs about a cent a day by default, and goes up as you drive more traffic or use more resources. I haven’t found my prices to be wildly fluctuating here, it’s always felt cheap.

As I mentioned, they are barebones. They do not have a ‘make-a-site’ wizard, or a chatbot that pops up to help you. They do have a deep, and helpful, FAQ and an active support forum. So many of the questions I had were answered quickly in either of these. They are also consistent and transparent. When you set up a site, you are getting a isolated segment on one of their servers, and this segment contains a number of folders you can place files within. Any file you put into the public folder will be accessible over the web. They then give you
the username and address you can use to log into this server when you want to bring those files over. The most direct way to log into your site is through your computer’s terminal.

At this point, this likely feels hella overwhelming. It was overwhelming for me! I find making coolguy involves having a simple idea that explodes into all these side trails of obscure words and forgotten tools. There isn’t a simple way to get around this, and following these side trails is one of the most pleasant parts of all this to me. It reminds me of discovering some band and finding out they are a part of a regional scene, with all these other bands and artists to discover and fall in love with too. Web building is like that, but with computer techniques instead of band names.

I recommend NearlyFreeSpeech because they are simple, if not immediately easy. They do not use unique, buzzy phrases or proprietary gloss to get your site up. They use standard and traditional concepts, which means putting a site up on NearlyFreeSpeech gives you skills needed to put up a site anywhere else.

Which is another important thing to know! Your domain name directs people to a place, it is not the place itself. You can always change your mind on who hosts your site, then point your domain to this new location. When you do this, your site will look no different from the front, while the backend processes can be changed completely. If there’s another hosting provider that works better for you, please use it (and tell me about it!)

It’s alright if this doesn’t make sense right now, as it likely doesn’t need to yet. The essential thing is that your web address is detached from where your website is, which is detached from how your website is made. You have freedom and choice all the way down.}

◊sect["good stuff" "writing the site"]{
This here is the heart of it all, the reason for being as they say!

Websites are just text stored in files, then organized in folders. There isn’t anything more to it than that. Of course, you can describe the all literature in the same way–and in both cases it’s not the text that matters, but the intention and expression through the text. But the idea that all of this (makes sweeping motion around the room) can spring from arranging text around space...blows my mind. spend my days at libraries holding up books, shouting “Are you seeing this? An A, a B, a C and then THIS? It’s ridiculous!” and getting properly shushed.

Computers made a lot more sense to me when I saw them as bodies of literature,so I perfer to work in a way where tool I use is text. I push up files with a typed command, I manipulate text through a language built for that. I do it all from within a computer terminal, so that the screen never changes–it is always a blank, black page I type my desires into and watch them manifest.

This all sounds lilke mystical woo woo, because it is! All of it is. The internet is animist and the world is a dream about a map. And I discovered I could tap into this power to...make a pastel page filled with pastel boxes and a dancing gif of myself, and to write diary entries about how sometimes I get sad. My visions are modest, but it still feels incredible to manifest them.

Since everything is text, the most important aspect for building a site is using something to edit that text. We in the business call these “text editors”. For your editor, you want something that will not leave its own marks on your words (which is something programs like google docs and msWord do). Whenever you see mention of “a text editor”, that’s all it means–a program that will edit any text it’s given, but do nothing else to it.

Text editors are a simple concept, but when you realize that so much of what makes your computer run is text, and you have a tool to edit any of that, it feels like receiving a master lockpicking set and computers are just a set of doors. You’ll start to edit html pages ot make a website, but it will seep out until you are editing configuration files so your computer calls you by name, or writing new incantations to make some complex act happen by typing a single,significant name.

I use the text editor Vim, which is a whole method and language and its own sacred path. It is free, and likely already installed on your computer, but it can be disorienting upon first use. There are other, simpler editors that come installed on your computer that are also good. If you are on a mac, search for ‘textEdit’ and just make sure you’re wrting in plaintext mode. If you’re on a windows, search for notepad. These programs are not fancy, and it’s incredible to know that something as basic as textEdit still has everything you need to
create your own website.

If you want something a bit more powerful, check out the editor atom. It’s free and built especially for coding, while remaining relatively simple. When using it, remember that you just need something to type text into. If there’s some button or feature of atom that’s confusing, just ignore it for now. It isn’t meant to be useful yet, and it will make sense when you need it to.

(And if you are on a mac or linux, and want to try the epic poem that is Vim,open up your Terminal program and type vimtutor. This will start the tutorial that makes flowers blossom in your head.)}

◊sect["heart" "languages"]{
Coolguy.website is written almost entirely in HTML and CSS. There is a tiny amount of javascript on the calendar page, but that is it. HTML and CSS are great because they are incredibly forgiving. Their purpose is to tell the browser the content and structure of your page, and it’s aesthetic. If you write HTML or CSS that the browser doesn’t understand, it just ignores it and renders everything the best it can. You can write supremely bad HTML and CSS and it will not break your site. I mean, your site may look funky, but the main content will still display. You’ll alsofind that with just a small amount of HTML/CSS knowledge you can quickly develop your own pattern and techniques to build an aesthetic that’s unique. You can learn these languages at your own rate, while still expressing what you want to express. This feels greeeeeat.

In my own path, I found that when I started I wrote a buuuunch of code to make the site do what I wanted, then learned how to pare the code down to its simplest core. I now try to write code without any flourish, as plain as possible, while still capturing an aesthetic. I love it, because I’m developing a writing style for the entire structure of the page, and not just the words visible on the screen.

The other thing that’s nice about these languages is that you can learn them through any other website. Right click on this page, for example, and choose ‘view source’. This is the entirety of code for making this page. You can do this for _any_ page you visit.}

◊sect["rsync" "getting your files online"]{
So now you’ll have a collection of beautiful HTML pages stored on your computer that you want to broadcast to the world. So you will need to sync these files to your server. If you’re using NearlyFreeSpeech, it means getting them into your server’s public folder.

There’s a couple fundamental ways to do this. One is to use the File Transfer Protocol, or FTP. There’s a free client for htis called Filezilla that is nice and straightforward. Their web page is unadorned, but the writing in the site is friendly and thorough.

Another option, and the one I use, is rsync. This is a terminal command that you can use to sync your local computer to a remote server. Rsync can be a bit disorienting when first trying it, but what’s nice is once you have a command that works you either just paste it in each time, or set it to a specific incantation in your terminal.

For example, my rsync command looks like this:

◊code{rsync -avP–delete–exclude=“.*“–exclude=“.*/“/ ~/Projects/Web/coolguy/
zcg@ssh.phx.nearlyfreespeech.net:/home/public’}

But I don’t type that every single time and can’t recite it by heart. I assigned an “alias” to this command, and type that alias instead. So whenever I want to sync my code, I simply type “coolguyPush” into my terminal and watch as the entire process is done for me.

I like rsync for the same reason I like nearlyfreespeech. It’s transparent about what it’s doing. Even if I dont’ understand all parts of it yet, I can learn them when I’m ready. Getting your files from your computer to your server is tricky. Or rather, the process reveals how opaque and strange all the internet and computing can be. There are services that make things further opaque with the goal of “user-friendliness”, abstracting what’s happening beneath the surface, so everything works with the click of a button. For me, my ultimate goal is clarity and simplicity, so I prefer the methods that illuminate even if they’re hard at first. I also will always choose the methods that teach me new words and traditions, over requiring some new service. Not only are these words and traditions free, but they empower me and connect me to a chain of webmasters that I find v. heartening.}

◊sect["conclusion" "gooooodbyyee"]{
That, in essence, is my flow for coolguy. In summary: I bought a domain and site from nearlyfreespeech. The domain cost me $12 a year,and the server costs about a cent a day. I use vim on my computer to write html and css files, and then I use the magic command “coolguyPush” to push these files from my local folder to my server’s public folder (with coolguyPush being an incantation to summon the rsync command).

This essay was not intended to be a step by step guide, so I would not expect you to be able to get a site up just by reading these sections. But I hope the general process makes a bit more sense and feels exciting, and that you have some paths you are eager to explore. If you have any specific questions on getting your site up, please feel free to email me cos I’d love to help!

And, of course, when you have a homepage, please invite me over!}
