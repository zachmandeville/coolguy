#lang pollen

◊define-meta[title]{About Coolguy.Website}
◊define-meta[background]{lavenderblush}
◊define-meta[color]{teal}


This is a homepage, that has been my home for several years.  When 'The Modern Coolguy' was made, I wrote an introduction that gives better context to what exactly this is, and why it is.  It's largely still relevant, but the site (and myself!) have changed pretty drastic since then!

◊big-link["introduction.html"]{Read the Introduction}

Now, I find myself with new dreams and directions for this home.  These inspired a styling overhaul for the page, and a new essay on my intentions.

My biggest dream is for all y'all to have your own homepages too, but I know that it can be confusing on where to start.  I've written up an overview for how I make my site (and how sites generally function) to help with that initial confusion.  Coolguy is intended as an instruction manual for homes, as much as my own home.

Lastly!  The site has gone through several stages, and several looks, and so I wrote up a lil history!

Thank you for checking out the site I hope you like it!!!!!!


