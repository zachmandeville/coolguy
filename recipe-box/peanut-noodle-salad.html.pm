#lang pollen

◊dish{Quick Peanut Noodles}

◊ingredients{1/4 cup Soy Sauce
             1/4 cup Water
             2-3 cloves Garlic (minced)
             2-3 Green Onions (diced)
             1 Tbsp Oyster Sauce
             1 Tbs Apple Cider Vinegar
             1 Tbs Hoisin Sauce
             2 eggs
             2-3 Tbs Chunky Peanut Butter
             1/4 tsp Ground Ginger
             1/8 tsp Red Pepper Flakes
             1/2 package Rice Noodles
             2 limes}

◊recipe{In a small saucepan, combine all the sauce ingredients.

        Heat over low heat until bubbly.

        Bring about 1/2 a pasta pot of water to a boil.

        Turn off the heat and add the noodles and veggies.

        Let sit for 10 minutes and then drain completely.

        Stir in sauce and serve.}

◊Source["https://www.brandnewvegan.com/recipes/easy-peanut-noodles" "external recipe page at brand new vegan"]
{Brand New Vegan}
