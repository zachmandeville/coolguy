◊define-meta[title]{Make a Webpage!}

◊h2{Introduction}
◊h2{What we'll make}
- A site with a basic set of texts and some links to headings on the page.
- An audio player, that plays a nice song and lets you download it.
- styles added to the top.
◊h2{Materials}
◊h2{First Page}
- go from fumbling to solid and without any notion of best practice.
- start with scratch, a blank page and just write in it.
- enough information to be dangerous, because life is danger.
◊h2{Resources}
◊h2{
