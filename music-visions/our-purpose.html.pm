#lang pollen

◊h1{Our Purpose}

  Now that we have an audience, we can better find the purpose of datradio.  We can name the great need that our passion and skills and datsites can meet.  And, because we are smart, we know that we won't have the answer for what that might be yet. Not only that, we are smart enough to know we don't yet have the questions for whatever answers we mean to find.

So, then, before we build a thing and before we exclaim our purpose, let's develop some good questions for why we should build.  Let's keep datradio a misty ideal in the distance-- an answer slowly finding its shape.

We could start with the question, "How do we replace soundcloud, using decentralized tech?", but we'll quickly find it is not useful. The question is implying the problem with music today is the tech stack on which its platforms are built; as if the thing artists want is for everything to be the same, but secretly built differently. Rebuilding the features of soundcloud with decentralized tech is an interesting artistic constraint for the coders, but not the artists we are coding for.  If you were to ask a musician what they'd change about spotify, they aren't going to say "It'd be cool if it was written in C".

If we ask the broader question, 'How do we share music in a decentralized way?' we'll also find it lacking.  The question posits art to be this abstract, detached thing; that we have this resource called ◊em{music} and our big problem today is not knowing how to tap into and share it.

But music is of course made by ◊em{people}.  You cannot abstract or detach music from its makers.  Well, you can...I mean, that's Spotify's thriving business model...but you would be evil and cosmically doomed to failure.

The answer to this question is simple: music is shared when people feel supported enough to ◊em{want} to share it.

A better question for us to start with is, "What is it like to be an independent musician today?"

Today, it is easier than ever to make music.  All the tools you need to make and record an album can be run on your personal laptop.  Once you have it recorded, everything to make your band logo, album art, and first music video is available on that same machine.  You can be a single vertical industry operating from a bedroom on a 12" screen.  And if you have trouble learning any aspect of the industry-standard software you pirated for free, then it's guaranteed that a kind, mysterious 13 year old has already uploaded a youtube tutorial on the exact thing you are trying to do.

We are in a phenomenal time for making music.  It's the sharing of it that's hard.

Upon releasing your music, there's a whole new set of tasks and responsibilities that have nothing to do with your art, instead driven by unknown algorithms on a handful of websites.  Making sense of how to effectively operate within these techies' rules feels obtuse and arcane, especially since their rules seem to shift and become harder every day.  It is easy to feel that platforms like Spotify and Apple Music are working against you, because they are.

To be clear, unless you are one of their in-house artists, or a major artist with the money to partner with the platform, ◊a[#:href "https://thebaffler.com/downstream/unfree-agents-pelly"]{Spotify is working against you}.  The company is aggresively working to make you and your support network obsolete.  Spotify wants to grow fanbases ◊a[#:href "https://watt.cashmusic.org/writing/thesecretlivesofplaylists"]{for their platform and their playlists only}.  ◊a[#:href "https://thebaffler.com/salvos/the-problem-with-muzak-pelly"]{Their explicit strategy is to promote 'passive listening'}, where music continually drifts by without any attached origin or culture.  This even leads to them developing ◊a[#:href "https://thebaffler.com/downstream/streambait-pop-pelly"]{their own genre of music}, aiming to snuff out original sound itself.

When you drop your song into the millions of other songs on Spotify, you are not competing for attention between other bands and sounds.  You are competing against the platform itself.  You've got a laptop.  They have millions of dollars and a hundreds of employees.  It's hard.

If you want your work to reach people through streaming platforms, you'll learn the best strategy is to promote it outside of these platforms.  Which means maintaining a strong, and unique, social media presence.

Unfortunately, like Spotify, these social platforms are also operating through secret algorithms that are actively working against you.

The chief purpose for social media platforms is to maintain eyes and attention on the platform itself.  Their revenue model is based on generalized attention and growth--it is not based on the success or happiness of any of its users. Art, culture, and emotional well-being do not matter to social platforms except as potential hooks for advertistements.

Twitter and Facebook's basic goal is to foster addiction disguised as conversation. And these sites have found that the most addictive type of conversation is straight-up conflict.

Let go long enough, any twitter exchange will reduce itself to a series of hot takes, binary sides on a sensitive issue, short barbs that seep with pain and toxicity.  As you spend time on these platforms, building and engaging with your fanbase, you'll find your posts slowly start to take on this twitter style.  You'll develop your own unique wit for dressing someone else down, or exclaiming how shitty you feel, or scorching something you disagree with. This style will be the one that persists above anything else.

If you gain followers and attention here, it is legitimately impressive. There are not a multitude of social platforms for all our wonderful interests and communities where your style is a natural fit.  There's like three networks, and they make up the majority of what is today considered 'the web'.  This means that as a musician trying to find your footing and fans online, you are not competing for attention with just other bands and musicians.  You are competing for attention with movie stars, tv shows, podcasts, presidents, and corporate juice brands pretending to be suicidal teens. 

This is tough work of course, and it's work you never meant to do. You didn't want to compete with bands and musicians for attention, and you definitely didn't want to compete with frozen pizza brands.   You wanted to share music and play music and meet your idols and make and inspire fans.  You wanted to support music and be supported by your music and for your music. But now hese support network have largely disappeared, or are as emotionally strained as you.

There are fewer and fewer online communities outside of these major platforms.  There are fewer music zines and blogs, and the ones that remain are also on twitter struggling with how hard it is to be online.  There are fewer record stores and record labels.  Even your favorite venues are being shut down through weird social media campaigns, like orchestrated efforts from alt-right 4chan threads to destroy as many diy venues as possible, simply to see if they can.

All of this is super depressing, which is a benefit because it gives you something to talk about on twitter...so you log on to write another strategically casual, on-brand tweet about how everything is awful. You exist within the toxicity until it absolutely depletes you, all because you wanted people to hear a song.

And to be clear, that is the simple and small point to sharing your music today.  You just want people to hear and like it. There is no dream of rock star riches and fame.  Sometimes you've entertained the idea of making enough money to pay for putting out the record, but wanting anything more more than that feels absurd.  Even the famous, lavish popstars on your feed seem to make their money off music-adjacent brand synergy; all wearing sponsored clothing while talking up a soda company on a patreon-supported twitch stream.

The notion of making money off music is silly, because music is just a thing you get for 9.99 a month.  Everyone's making music.  You get an email of 70 new musics each week from spotify.  There's no human pattern to what they send, no exploring a scene, a label, the evolution of an artist.  Just 70 new things you have 7 days to listen to before the next 70 arrive.

Your monthly music feed feels like a power bill, and you don't assume each penny towards power goes to some specific electron pulse.  Songs, like electrical currents, are just anonymous things that form into a stream you feed into your home.  Tiny electric you is not going to make money off your songs, and that's fine.  It's not about that.  

But, since you aren't making money here, you have to make money in other ways and, unfortunately, the whole economy is in the shitter.

No one is doing well except for the heads of the tech companies that are exploiting you.  You and your musician friends are all working multiple jobs just to live and maybe make enough to afford to splurge on the next live show. 

You walk dogs on the way to your coffee job, then come home to do freelance writing and online transcription before running off to a show.  The most stable income you have is selling the socks you wore as you ran around. There's a man in Texas who will pay a lot for your long day smell; reliably so.

Your band might be able to tour, get on year-end lists, have a viral tiny desk concert video--and you still have to break up because no one can afford the time off work anymore.

Or you may be one of the fortunate few who came at the right time, and there was some confluence of luck, tech, and taste that you are kind of 'making it'.  You have far more followers than you ever could imagine, you are supporting yourself through your music, and your work is loved by and inspiring the right people.  But you find this gives you no actual relief.


Maybe find that you are automating a little bit, but only with your art.  Your songs are starting to sound more like 'spotify-core', you are listening too much to the brands and algorithms instead of yourself, instead of the little voice that first made you sing.

At your last band practice, you discussed how to subtly shift the brand and narrative each of you needed to maintain on your individual and group accounts to keep the band relevant.  You barely picked up your instruments.

So you stop making music.  It's not due to artistic differences with your bandmates, or rockstar excess.  It's not because you have no fans or outlets.  You have more fans than you ever thought possible. But you can't do this anymore because you are ◊em{tired}.

You are tired of having to relentlessly push content ancillary to your passion, maintaining multiple identities and profiles and narratives for your fans.  Of having hot, anxious sleep because you sent out a tweet right before bed, to hit your band's agreed quota, and you're worried of its response.  You're tired of having to maintain twitter discussions with your abusers, of not knowing which insta posts will lead to new abusers.  You're tired from getting done with a show at 1am, and having to be at work making coffee at 6am.

Your heart and body are exhausted by everything except your music.  But music is what you're known for, and stopping your music will let you stop everything else.

This is the main problem facing musicians today: a spreading, indefinable, all-consuming and all-exhausting heartbreak.

If we want to make technology for artists, if we want to have and share music in a new decentralized way, then our core questions should be this:

"How do we make it so sharing your art isn't a mental health risk?"

Or:

"How do we foster support instead of division?  How can we establish trust again between art and technology, where our work helps sustain an artist to keep creating, instead of making them feel crushed by everything they loved?"

A new platform won't give us the answer.  An interesting tech stack won't provide the answer either.

It will take empathy, and righteous anger, and shame at what the previous hot new technologies have caused.

This is our plain and simple purpose: 'Help make life less sad, by making the sharing of creation fun again."

How do we do this?  By being human beings who care about other human beings, and looking to the real communities, people, and art that raised us. Our code should be as human as the songs and people radiating through it.
