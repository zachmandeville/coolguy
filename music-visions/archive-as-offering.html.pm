#lang pollen
◊h1{Archive As Offering}

We are calling this collection of code you are reading now a zine.  It's not the official name, it's just that writing it felt alot like writing a paper zine.

You are likely reading this in a browser, and the url starts with ◊code{dat://}.  So you might call this zine a 'dat site', but that name is also not official, it's just the overall appearance and feel of reading html in a browser reminds us of an old-fashioned website.

According to the Dat protocol, The technical name for this thing you're reading is an ◊em{Archive}. I'm not fully sure why it's called that, except I guess the people who made it were reminded of archives in some way, and it stuck.

The reason for the name doesn't really matter.  Given enough time and use, the word will no longer hold any tie to its origin.  It will become fully phonetic, like zine and site, where the sound itself describes the thing without having to be tied to any specific physical resemblance.

To get a better sense of what this ◊em{is}, instead of what it's ◊em{named}, it might be good to describe how it is that you're reading it at all, and why I've shared it with you.

So hello, dear reader, this is Zach! The friendly author!  I want to run through a thought with you, and share a bit of vulnerability.

And because we are in the future, there is both vulnerability and ◊em{techno vulnerability}.

~~*

I am writing this to be read by more people than just you.  I think you're cool, but I have a hope that these words are useful enough that people will want to share them widely. To be honest, I write this with the hope that I don't know you at all.  You are a friend of a friend of a friend I haven't met yet, and my words will be our introduction before we may ever meet.

But it's likely that I ◊em{do} know you, because you are the ◊em{first} reader fo these words, the first person I shared this datlink with.  And it's likely that I'm sitting across from you now.  Which means that as you read this, I am staring at the back of your computer screen and thinking, "I really hope they like this enough to share".

That was the vulnerable part.

This is not a website, which means we are not on the web at all.  We are somewhere different, a place some call the Chorus.  And the methods and traditions in the Chorus are different than the web.

For example: to build this archive|site|zine, I made a folder on my computer called `music-visions`, and within this made a file called "archive-as-offering.html".

Then I opened Beaker Browser and offered it this personal folder, to share all these files and folders with you.


When I shared this link with you, I wasn't sharing a link to some web server, or blog service, or social platform.  You are reading all of this because right now I  have my computer open, actively sharing it.  Our two screens are connected by a living, ethereal thread.  

You might be at the table with me, or in the room next to me, or on another continent( editor's note: I dont' know who I'll share this with first.)

Wherever you are, you can know that after I shared the link with you,  I stayed in front of my computer, beaker browser open, continually expressing my words through the ether.  I kept the computer on so I could see '1 peer' appear in my browser window, letting me know you were now in my computer, in my folder, looking through the files I prepared as an offering to you.

That was the techno-vulnerable part.

~~*

On the web, we submit our creations to central servers and then ask these servers to share our work with others.  The web is sort of a vast library, where we can place our own books on the shelves and let others know in which wing, and on which shelf, you can find our writing (trusting that the library didn't remove that wing, or reorganize that shelf, or just plain shut down.)

But here, in the future, everything's more personal.  I am not depositing a book to a library, hoping you'll discover it.  I am handing you all my pages at once, at the cafe table we are both sitting at, and trying to not look nervous as you read through all the words.  I am hiding the hope that you pore through them twice, then ask, "Can I share this?"

And so maybe you are not the first reader, you are just ◊em{you} the person I don't know.  Perhaps "Zach" is another word like "zine" or "site", a sound that holds no physical resemblance.  If that's the case, it means the wish I offered here has been fulfilled, my words have been shared enough that they could finally reach you.

Now, imagine that I was not offering this meta-set of words. Instead, I was offering the song I'd just written, the album I just finished, the mix I'd just made for you.

And imagine you have all this power too. That if you wanted to make a similar wish you can copy all of this code with the click of a button, make the words your own, append your own song, andfind your first reader.

This is not a website, or archive, or zine.  It is something different.
